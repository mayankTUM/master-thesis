\chapter{Case Study And Results}\label{chapter:results}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/big_example_la.png}
	\caption{Logical architecture of Industrial-like use case} 
	\label{big_example_la}
\end{figure}

So far we have seen an example that is good enough to explain how all the constraints work. Considering the complexity of modern safety critical systems, the trend is for these architectures to become system of systems, where multiple functions are delivered by complex networked architectural topologies and where functions can share components~\cite{Knight:2002:SCS:581339.581406}.  Figure \ref{big_example_la} shows a complex logical architecture having 48 components, each with their own safety and memory requirements. \\


The safety requirement of all the components in the technical architecture is shown in the table \ref{table:asil_big_example_la}. The memory required by each component is assumed to be 10 units. Also, the duration of each logical component is assumed to be 10 units.\\

\begin{table}[h!]
	\centering
	\def\arraystretch{1}
	\begin{tabular}{ | l | l |}
		\hline
		ASIL Level & Components \\ \hline
		ASIL D & c1, c2, c3, c4, c5\\ \hline  
		ASIL C & c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17 \\ \hline  
		ASIL B & c18, c19, c20, c21, c22, c23, c24, c25, c26, c27 \\ \hline
		ASIL A & c28, c29, c30, c31, c32, c33, c34, c35, c36, c37, c38, c39  \\ \hline
		QM  & c40, c41, c42, c43, c44, c45, c46, c47, c48 \\ \hline
	\end{tabular}
	\caption{ASIL levels of logical components of industry-like use case}
	\label{table:asil_big_example_la}
\end{table}

\section{Phase I Results}

For phase I, wherein the deployment and schedule generation process of AutoFOCUS3 is extended with safety metrics and resource constraint, we show how these constraints come into play while generating a valid deployment. For this, we need to define a technical architecture with resource constraints for each hardware component. The technical architecture is shown in the figure \ref{big_example_ta} and the properties of the hardware components are defined in the table \ref{table:properties_big_example_ta}. \\


 
\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/big_example_ta.png}
	\caption{Technical architecture of Industrial-like use case} \label{big_example_ta}
\end{figure}

\begin{table}[h!]
	\centering
	\def\arraystretch{1}
	\begin{tabular}{ | l | l | l | l | l | l | l | l | l | l|}
		\hline
		Model Element & ECU1 & ECU2 & ECU3 & ECU4 & ECU5 & ECU6 & ECU7 & ECU8 & ECU9  \\ \hline
		PMHF (in FIT) & 5 & 5 & 100 & 100 & 500 & 500 & 2000 & 2000 & N/A \\ \hline  
		ASIL & D & D & C & C & B & B & A & A & QM \\ \hline  
		Cost & 10 & 10 & 8 & 8 & 5 & 5 & 3 & 3 & 1 \\ \hline
	\end{tabular}
	\caption{Properties of technical architecture of Industry-like use case}
	\label{table:properties_big_example_ta}
\end{table}

Each ECU has been allocated a memory of 100 units. This means, an ECU can carry at most 10 logical components, since each of them require a memory of 10 units. \\ 

In the following section we analyze the effects of reducing the overall cost of technical architecture on the generated deployment. Also, we analyze the effects of using \textit{node usage constraint} in deployment generation and how it is different from the \textit{cost constraint}. Section \ref{5.1.1} shows the results of using the cost constraint  and section \ref{5.1.2} shows the results of using node usage constraint. Section \ref{5.1.3} shows the effect of cost on E2E latency and section \ref{5.1.4} shows the effect of cost on Bus load (No. of bus signals). \\

\subsection{Cost Constraint} \label{5.1.1}

For analyzing the effects of Cost Constraint on the generated deployment, we assume two different cases, one where the cost is non-uniform and the other where the cost is uniform. The former indicates the fact that developing a more stringent node in terms of safety and requirements, requires more development effort and time as compared to developing a less stringent node. The latter case, where the cost is uniform, makes it interesting to analyze which nodes are kicked out of the deployment first, since different stringent nodes are assumed to have same development cost.\\

Reducing the maximum cost allowed will lead to reduction in number of nodes used, but which nodes are removed from the deployment generation is worth analyzing. Assuming the PMHF constraint, BUS constraint and Memory constraint hold, we now analyze the effect of cost reduction. \\
 
Figure \ref{result_1} shows a plot of maximum cost allowed vs no. of nodes used in the deployment. The BUS is kept as ASIL-D compliant. By specifying the maximum cost allowed for a deployment generation, \textit{cost constraint} forces the Z3 solver to find a deployment that do not exceed this cost. The graph shows that by reducing the max cost, we have been able to find a deployment that do not violate any of the constraints and reduces the node usage, hence reducing the overall development cost of the hardware architecture. For e.g. for a maximum cost of 53 we have a deployment consisting of 1 QM, 2 ASIL-A, 2 ASIL-B, 2 ASIL-C and 2 ASIL-D ECUs, whereas for a maximum cost of 37, we have a deployment consisting of 1 QM, 1 ASIL-A, 1 ASIL-B, 1 ASIL-C and 2 ASIL-D ECUs. \\

\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/result_1.png}
	\caption{Effect of Cost Constraint (non-uniform cost) on deployment (ASIL-D BUS)}
	\label{result_1}
\end{figure}

Figure \ref{result_2} illustrates the scenario where the development costs of nodes with different stringencies are assumed to be same (In our case, 6). Since the cost is uniform, the Cost constraint can kick out any node from the deployment generation process. For e.g. for maximum cost of 37, the deployment we got has 1 ASIL-A, 2 ASIL-B, 1 ASIL-C, and 2 ASIL-D nodes. But if we compare it with figure 51, we have a deployment containing 1QM, 1 ASIL-A, 1 ASIL-B, 1 ASIL-C and 2 ASIL-D. This shows the impact of using different cost heuristics in generating a deployment that optimizes the development cost and efforts. Similar analysis has been done by Azevedo et al. in~\cite{azevedo2014exploring}.\\


\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/result_2.png}
	\caption{Effect of Cost Constraint (uniform cost) on deployment (ASIL-D BUS)}
	\label{result_2}
\end{figure}

Effect of non-uniform and uniform cost for ASIL-C BUS are shown in figure \ref{result_3} and figure \ref{result_4} respectively. There is no deployment possible if the Bus is kept as ASIL-B compliant or lower. This is because, an ASIL-B bus forces the component c1-c17 to be deployed onto the same ECU, but the memory constraints do not allow any ECU to carry more than 10 tasks.\\

\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/result_3.png}
	\caption{Effect of Cost Constraint (non-uniform cost) on deployment (ASIL-C BUS)}
	\label{result_3}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/result_4.png}
	\caption{Effect of Cost Constraint (uniform cost) on deployment (ASIL-C BUS)}
	\label{result_4}
\end{figure}
 

\subsection{Node usage constraint}
\label{5.1.2}

To illustrate the effect of this constraint on the deployment generation process, we have kept Maximum cost allowed to a fixed value, say 54. Instead of reducing the max cost allowed, we will reduce the maximum no. of nodes in each iteration. Figure \ref{result_5} shows the effect of this constraint on the generated deployments. Comparing with figure \ref{result_1}, for 6 nodes, the best deployment that it can find contains 1 ASIL-A, 2 ASIL-B, 1 ASIL-C and 2 ASIL-D leading to a cost of 41, whereas the best deployment found by \textit{cost constraint} for 6 nodes includes 1 QM, 1 ASIL-A, 1 ASIL-B, 1 ASIL-C and 2 ASIL-D, yielding a cost of 37. So \textit{cost constraint} can find a deployment using as many nodes as used by \textit{node usage constraint}, but with a lower technical architecture development cost. \\

\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/result_5.png}
	\caption{Number of nodes constraint (ASIL-D Bus)}
	\label{result_5}
\end{figure}

Figure \ref{result_6} shows the similar analysis for ASIL-C bus. Comparing it with figure \ref{result_3}, the best deployment found by \textit{node usage constraint} for 6 nodes includes 1 QM, 1 ASIL-A, 2 ASIL-C and 2 ASIL-D yielding a cost of 40, whereas the best deployment found by \textit{cost constraint} for 6 nodes includes 1 QM, 2 ASIL-A, 1 ASIL-C and 2 ASIL-D, yielding a cost of 35.\\

\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/result_6.png}
	\caption{Number of nodes constraint (ASIL-C Bus)}
	\label{result_6}
\end{figure}

This indicates how \textit{cost constraint} can outdo the \textit{node usage constraint} by not only finding a deployment that avoids unnecessary usage of the nodes, but also by finding a deployment that optimizes the cost by using less stringent ASIL nodes wherever necessary.\\

\subsection{End-2-end latency vs cost}
\label{5.1.3}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/result_7.png}
	\caption{E2E latency vs Cost}
	\label{result_7}
\end{figure}

Figure \ref{result_7} shows a plot between the E2E latency and the maximum cost allowed. Since the reduction in the cost reduces the number of the nodes used in the deployment generation, this, therefore, leads to a rise in latency as there will be more tasks scheduled to run on a single ECU. In other words, the degree of parallelism to task scheduling will be reduced.\\

\subsection{Bus load vs Cost}
\label{5.1.4}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=100mm,scale=0.8]{figures/result_8.png}
	\caption{Bus load vs Cost}
	\label{result_8}
\end{figure}


Figure \ref{result_8} shows a plot between the cost and the bus load. Currently in AutoFOCUS3, the bus load is measured in terms of number of bus signals that are sent between the components over the bus. More the bus signals, more the bus load. The plot indicates that the bus load reduces with the number of nodes since more components are deployed to the same ECU, but this might not hold true always. It depends on the distribution of the components onto the nodes. If the communicating components are allocated to same ECU, the number of bus signals reduces. In other words, if the communicating components are allocated to different ECUs, the number of bus signals increases. \\


\section{Phase II Results}

In this section, we show the results of using new MOO algorithm for generating the pareto-optimal deployments for the logical arhitecture shown in figure \ref{big_example_la}. Assuming the values of the variables \textit{max\_nodes}, \textit{max\_memory}and  \textit{upper\_bound\_sil} entered by the user are 9, 100 and 36 respectively, the set of valid deployments generated are shown in table \ref{table:valid_dep_big_use_case}. For such a large use-case, there are some instance of constraint satisfaction problem that the Z3 solver cannot decide as \textit{satisfiable} or \textit{unsatisfiable} in limited amount of time. We have used a timeout of 6 hrs for each instance of the problem and yet there are some \textit{undecided} instances of the problem due to obvious limitations of the SMT solvers. Hence using different timeouts might generate different sets of valid solutions, resulting in different sets of pareto-optimal solutions.

 
%\begin{table}[h!]
%	\centering

{\footnotesize  % Switch from 12pt to 11pt; otherwise, table won't fit
%\setlength\LTleft{-30pt}            % default: \fill
%\setlength\LTright{-30pt}           % default: \fill
\begin{longtable}{@{\extracolsep{\fill}}lllll@{}}
	
%	\def\arraystretch{1}
%	\begin{tabular}{ | l | l | l | l | l |}
		\hline
		Number of nodes & Memory per node & E2E latency & TotalSIL & BusSIL \\ \hline
		5   & 100 &	504	&   19 &	4 \\ \hline
		5	& 100 & 401 &	18 &	4 \\ \hline
		5	& 100 & 410 &	17 &	4 \\ \hline
		5	& 100 & 425 &	16 &	4 \\ \hline
		5	& 100 & 439 &	15 &	4 \\ \hline
		5	& 100 & 373 &	14 &	4 \\ \hline
		5	& 100 & 384 &	13 &	4 \\ \hline
		5	& 100 & 410 &	12 &	3 \\ \hline
		5	& 100 & 303 &	11 &	3 \\ \hline
		5	& 100 & 263 &	10 &	3 \\ \hline
		6	&  80 & 335 &	23 &	4 \\ \hline
		6	&  80 & 499 &	22 &	4 \\ \hline
		6	&  80 & 528 &	21 &	4 \\ \hline
		6	&  80 & 474 &	20 &	4 \\ \hline
		6	&  80 & 496 &	19 &	4 \\ \hline
		6	&  80 & 494 &	18 &	4 \\ \hline
		6	&  80 & 519 &	17 &	4 \\ \hline
		6	&  80 & 463 &	16 &	4 \\ \hline
		6	&  80 & 325 &	15 &	4 \\ \hline
		6	&  80 & 279 &	14 &	3 \\ \hline
		6	&  80 & 332 &	13 &	3 \\ \hline
		6	&  90 & 408 &	23 &	4 \\ \hline
		6	&  90 & 340 &	22 &	4 \\ \hline
		6	&  90 & 422 &	21 &	4 \\ \hline
		6	&  90 & 340 &	20 &	4 \\ \hline
		6	&  90 & 486 &	19 &	4 \\ \hline
		6	&  90 & 478 &	18 &	4 \\ \hline
		6	&  90 & 475 &	17 &	4 \\ \hline
		6	&  90 & 463 &	16 &	4 \\ \hline
		6	&  90 & 362 &	15 &	4 \\ \hline
		6	&  90 & 463 &	14 &	4 \\ \hline
		6	&  90 & 391 &	13 &	3 \\ \hline
		6	&  90 & 457 &	12 &	3 \\ \hline
		6	& 100 & 419 &	23 &	4 \\ \hline
		5	& 100 &	383 &	20 &	4 \\ \hline
		5	& 100 &	383 &	19 &	4 \\ \hline
		5	& 100 &	338 &	18 &	4 \\ \hline
		5	& 100 &	354 &	16 &	4 \\ \hline
		5	& 100 &	411 &	16 &	4 \\ \hline
		6	& 100 &	359 &	15 &	4 \\ \hline
		6	& 100 &	420 &	14 &	4 \\ \hline
		5	& 100 &	426 &	13 &	4 \\ \hline
		5	& 100 &	347 &	12 &	4 \\ \hline
		5	& 100 &	387 &	11 &	3 \\ \hline
		5	& 100 &	683 &	10 &	3 \\ \hline
		7	&  70 &	321 &	28 &	4 \\ \hline
		7	&  70 &	348 &	27 &	4 \\ \hline
		7	&  70 &	349 &	26 &	4 \\ \hline
		7	&  70 &	348 &	25 &	4 \\ \hline
		7	&  70 &	320 &	24 &	4 \\ \hline
		7	&  70 &	322 &	23 &	4 \\ \hline
		7	&  70 &	318 &	22 &	4 \\ \hline
		7	&  70 &	333 &	21 &	4 \\ \hline
		7	&  70 &	322 &	20 &	4 \\ \hline
		7	&  70 &	305 &	19 &	4 \\ \hline
		7	&  70 &	383 &	18 &	4 \\ \hline
		7	&  70 &	344 &	17 &	4 \\ \hline
		7	&  70 &	355 &	16 &	4 \\ \hline
		7	&  70 &	327 &	15 &	4 \\ \hline
		7	&  70 &	292 &	14 &	3 \\ \hline
		6	&  80 &	286 &	24 &	4 \\ \hline
		7	&  80 &	272 &	23 &	4 \\ \hline
		6	&  80 &	347 &	20 &	4 \\ \hline
		6	&  80 &	305 &	21 &	4 \\ \hline
		6	&  80 &	310 &	20 &	4 \\ \hline
		6	&  80 &	316 &	19 &	4 \\ \hline
		6	&  80 &	288 &	18 &	4 \\ \hline
		6	&  80 &	294 &	17 &	4 \\ \hline
		6	&  80 &	248 &	16 &	4 \\ \hline
		7	&  80 &	306 &	15 &	4 \\ \hline
		6	&  80 &	257 &	14 &	3 \\ \hline
		6	&  80 &	360 &	13 &	3 \\ \hline
		6	&  90 &	278 &	22 &	4 \\ \hline
		6	&  90 &	369 &	21 &	4 \\ \hline
		6	&  90 &	344 &	20 &	4 \\ \hline
		7	&  90 &	335 &	19 &	4 \\ \hline
		6	&  90 &	331 &	18 &	4 \\ \hline
		7	&  90 &	313 &	17 &	4 \\ \hline
		7	&  90 &	310 &	16 &	4 \\ \hline
		7	&  90 &	425 &	15 &	4 \\ \hline
		6	&  90 &	316 &	14 &	4 \\ \hline
		7	&  90 &	400 &	13 &	3 \\ \hline
		7	&  90 &	350 &	12 &	3 \\ \hline
		6	& 100 &	247 &	22 &	4 \\ \hline
		7	& 100 &	246 &	21 &	4 \\ \hline
		7	& 100 &	249 &	20 &	4 \\ \hline
		6	& 100 &	257 &	19 &	4 \\ \hline
		7	& 100 &	284 &	18 &	4 \\ \hline
		5	& 100 &	350 &	17 &	4 \\ \hline
		6	& 100 &	273 &	16 &	4 \\ \hline
		5	& 100 &	354 &	15 &	4 \\ \hline
		7	& 100 &	273 &	14 &	4 \\ \hline
		5	& 100 &	295 &	13 &	4 \\ \hline
		5	& 100 &	310 &	12 &	4 \\ \hline
		6	& 100 &	324 &	11 &	3 \\ \hline
		5	& 100 &	539 &	10 &	3 \\ \hline
		8	&  60 &	405 &	29 &	4 \\ \hline
		8	&  60 &	341 &	28 &	4 \\ \hline
		8	&  60 &	349 &	27 &	4 \\ \hline
		8	&  60 &	347 &	26 &	4 \\ \hline
		8	&  60 &	390 &	25 &	4 \\ \hline
		8	&  60 &	368 &	24 &	4 \\ \hline
		8	&  60 &	329 &	23 &	4 \\ \hline
		8	&  60 &	339 &	22 &	4 \\ \hline
		8	&  60 &	344 &	21 &	4 \\ \hline
		8	&  60 &	341 &	20 &	4 \\ \hline
		8	&  60 &	347 &	19 &	4 \\ \hline
		8	&  60 &	572 &	18 &	3 \\ \hline
		8	&  70 &	432 &	30 &	4 \\ \hline
		8	&  70 &	303 &	26 &	4 \\ \hline
		7	&  70 &	321 &	25 &	4 \\ \hline
		7	&  70 &	302 &	24 &	4 \\ \hline
		7	&  70 &	411 &	22 &	4 \\ \hline
		7	&  70 &	365 &	21 &	4 \\ \hline
		8	&  70 &	377 &	20 &	4 \\ \hline
		7	&  70 &	344 &	19 &	4 \\ \hline
		7	&  70 &	343 &	18 &	4 \\ \hline
		7	&  70 &	335 &	17 &	4 \\ \hline
		7	&  70 &	384 &	16 &	3 \\ \hline
		7	&  70 &	332 &	15 &	4 \\ \hline
		7	&  70 &	369 &	14 &	3 \\ \hline
		8	&  80 &	348 &	29 &	4 \\ \hline
		7	&  80 &	283 &	26 &	4 \\ \hline
		7	&  80 &	283 &	25 &	4 \\ \hline
		6	&  80 &	310 &	23 &	4 \\ \hline
		7	&  80 &	336 &	23 &	4 \\ \hline
		7	&  80 &	278 &	22 &	4 \\ \hline
		6	&  80 &	338 &	21 &	4 \\ \hline
		6	&  80 &	285 &	20 &	4 \\ \hline
		8	&  80 &	343 &	19 &	4 \\ \hline
		6	&  80 &	301 &	18 &	4 \\ \hline
		8	&  80 &	376 &	17 &	4 \\ \hline
		7	&  80 &	315 &	16 &	4 \\ \hline
		7	&  80 &	332 &	15 &	4 \\ \hline
		7	&  80 &	336 &	14 &	3 \\ \hline
		6	&  80 &	359 &	13 &	4 \\ \hline
		6	&  90 &	358 &	21 &	4 \\ \hline
		6	&  90 &	339 &	20 &	4 \\ \hline
		7	&  90 &	379 &	19 &	4 \\ \hline
		7	&  90 &	382 &	18 &	4 \\ \hline
		6	&  90 &	333 &	17 &	4 \\ \hline
		6	&  90 &	367 &	16 &	4 \\ \hline
		6	&  90 &	409 &	15 &	4 \\ \hline
		7	&  90 &	361 &	14 &	4 \\ \hline
		6	&  90 &	425 &	13 &	4 \\ \hline
		6	&  90 &	462 &	12 &	3 \\ \hline
		6	&  90 &	210 &	11 &	3 \\ \hline
		7	& 100 &	361 &	27 &	4 \\ \hline
		6	& 100 &	285 &	22 &	4 \\ \hline
		6	& 100 &	303 &	20 &	4 \\ \hline
		6	& 100 &	285 &	20 &	4 \\ \hline
		5	& 100 &	327 &	19 &	4 \\ \hline
		7	& 100 &	287 &	18 &	4 \\ \hline
		7	& 100 &	332 &	17 &	4 \\ \hline
		5	& 100 &	372 &	16 &	4 \\ \hline
		7	& 100 &	373 &	15 &	4 \\ \hline
		6	& 100 &	372 &	14 &	4 \\ \hline
		7	& 100 &	285 &	13 &	3 \\ \hline
		6	& 100 &	394 &	12 &	4 \\ \hline
		7	& 100 &	310 &	11 &	4 \\ \hline
		8	& 100 &	330 &	10 &	3 \\ \hline
		9	&  60 &	363 &	31 &	4 \\ \hline
		8	&  60 &	377 &	30 &	4 \\ \hline
		8	&  60 &	389 &	29 &	4 \\ \hline
		8	&  60 &	388 &	28 &	4 \\ \hline
		8	&  60 &	392 &	27 &	4 \\ \hline
		8	&  60 &	404 &	26 &	4 \\ \hline
		9	&  60 &	398 &	25 &	4 \\ \hline
		9	&  60 &	381 &	24 &	4 \\ \hline
		9	&  60 &	398 &	23 &	4 \\ \hline
		9	&  60 &	400 &	22 &	4 \\ \hline
		9	&  60 &	418 &	21 &	4 \\ \hline
		8	&  60 &	366 &	20 &	4 \\ \hline
		8	&  60 &	395 &	19 &	4 \\ \hline
		8	&  60 &	369 &	18 &	3 \\ \hline
		8	&  60 &	269 &	17 &	3 \\ \hline
		7	&  70 &	423 &	26 &	4 \\ \hline
		7	&  70 &	384 &	23 &	4 \\ \hline 
		7	&  70 &	384 &	22 &	4 \\ \hline
		8	&  70 &	405 &	21 &	4 \\ \hline
		8	&  70 &	401 &	20 &	4 \\ \hline
		7	&  70 &	414 &	19 &	4 \\ \hline
		7	&  70 &	390 &	18 &	4 \\ \hline
		8	&  70 &	402 &	17 &	4 \\ \hline
		7	&  70 &	392 &	16 &	3 \\ \hline
		8	&  70 &	425 &	15 &	4 \\ \hline
		8	&  80 &	287 &	29 &	4 \\ \hline
		6	&  80 &	353 &	20 &	4 \\ \hline
		7	&  80 &	492 &	19 &	4 \\ \hline
		7	&  80 &	530 &	18 &	4 \\ \hline
		8	&  80 &	453 &	17 &	4 \\ \hline
		8	&  80 &	459 &	16 &	3 \\ \hline
		7	&  80 &	401 &	15 &	4 \\ \hline
		8	&  80 &	294 &	14 &	4 \\ \hline
		6	&  80 &	281 &	13 &	3 \\ \hline
		8	&  90 &	419 &	29 &	4 \\ \hline
		6	&  90 &	370 &	19 &	4 \\ \hline
		6	&  90 &	415 &	18 &	4 \\ \hline
		8	&  90 &	395 &	17 &	4 \\ \hline
		6	&  90 &	425 &	16 &	4 \\ \hline
		7	&  90 &	418 &	15 &	4 \\ \hline
		6	&  90 &	420 &	14 &	4 \\ \hline
		8	&  90 &	192 &	13 &	3 \\ \hline
		7	&  90 &	252 &	12 &	3 \\ \hline
		8	& 100 &	422 &	28 &	4 \\ \hline
		6	& 100 &	361 &	17 &	4 \\ \hline
		8	& 100 &	313 &	16 &	4 \\ \hline
		7	& 100 &	325 &	15 &	4 \\ \hline
		8	& 100 &	408 &	14 &	4 \\ \hline
		7	& 100 &	395 &	13 &	4 \\ \hline
		7	& 100 &	522 &	12 &	3 \\ \hline
		6	& 100 &	391 &	11 &	3 \\ \hline
		8	& 100 &	382 &	10 &	3 \\ \hline
		   
%	\end{tabular}
	\caption{Valid deployments for industry-like use case}
	\label{table:valid_dep_big_use_case}
%\end{table}
\end{longtable}
}    



The pareto-optimal deployments for this model are shown in the table \ref{table:pareto_dep_big_use_case}


\begin{table}[h!]
	\centering
	\def\arraystretch{1}
	\begin{tabular}{ | l | l | l | l | }
		\hline
		Number of nodes & Memory per node & E2E latency & TotalSIL \\ \hline
		5 &	100 & 263 &	10 \\ \hline
		7 &  70 & 292 &	14 \\ \hline
		6 &  80 & 248 &	16 \\ \hline
		6 &  80 & 257 &	14 \\ \hline
		6 &  90 & 210 &	11 \\ \hline
		8 &  60 & 269 &	17 \\ \hline
		6 &  80 & 281 &	13 \\ \hline
		8 &  90 & 192 &	13 \\ \hline
		
	\end{tabular}
	\caption{Pareto deployments for industry-like use case}
	\label{table:pareto_dep_big_use_case}
\end{table}


For such a huge industry-like use case, the number of valid deployments are 213. However, our MOO algorithm reduces the set of valid deployments to a set of 8 pareto-optimal deployments, thereby, helping the user in choosing a deployment from less, but better alternatives. \\





