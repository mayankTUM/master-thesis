\chapter{Introduction}\label{chapter:introduction}

\section{Background}


Safety-critical systems are those whose failure can lead to consequences that are determined to be unacceptable. The intuitive concern with such systems is the ramification of failures, which may lead to loss of life, potential damage of property or damage to the environment~\cite{Knight:2002:SCS:581339.581406}. Some examples of safety-critical systems are flight-control systems, automotive drive-by-wire, nuclear reactor management, operating room lung bypass machines, etc. In context of embedded systems, the term $safety-critical$ is a characteristic that ensures their functional safety in case of faults and failures.\\  

An essential step during the design phase of engineering of embedded systems is $\textit{Design Space Exploration}$. During this step, designers have to decide between many design alternatives, representing a multi-criteria decision problem. One such design space exploration problem is $\textit{deployment and schedule generation}$, which includes the mapping of logical components to the underlying hardware architecture, respecting various deployment constraints, plus generating feasible schedules to execute the components. \\

Developing Safety-Critical embedded systems requires developers to use certain tools and development techniques as suggested by appropriate standards. One such standard is ISO 26262, that ensures functional safety of electrical and electronic systems in road vehicles. Complexity of the E/E systems used in road vehicles has increased a great deal due to recent advancements in automotive applications. The use of centralized hardware architectures and the fact that they suffer from random hardware failures~\cite{SAFE_D333b}, complicates the process of ensuring safe and reliable operation of such applications.\\

AutoFOCUS3 (http://af3.fortiss.org), developed at Fortiss GmbH, is a research CASE tool that allows modeling and validating concurrent, reactive, distributed, timed systems on the basis of formal semantics~\cite{Hoelzl10autofocustool}. One of the many functionality of AutoFOCUS3 is generating deployments and schedules for mixed-critical shared-memory embedded systems~\cite{6601578}. Extending the deployment and schedule generation functionality of AutoFOCUS3 with the safety metrics identified in ISO 26262 and various resource constraints such as memory per node, power consumption etc., will, therefore, help in generating safer and more reliable deployments and schedules.\\

However, for a pragmatic system with a large design space, there may be multiple feasible deployments that satisfy various safety and resource constraints. It is, therefore, desirable to have some optimization criteria that further reduces the $\textit{design space}$ and leads to $\textit{optimal}$ (pareto-optimal) deployments.\\ 

Hence, one part of the thesis is aimed at extending AutoFOCUS3's deployment and schedule generation process with safety metrics and resource constraints, while the other part is aimed at introducing a simple MOO algorithm that optimizes multiple criteria (number of nodes, memory per node, power consumption, etc.) to yield an optimal (pareto-optimal) deployments.


\section{Problem Statement}

We are analyzing the possibility of extending the deployment and schedule generation functionality of AutoFOCUS3 with different safety metrics and resource constraints identified in ISO 26262, and to come up with a MOO approach that leads to pareto-optimal deployments. The real-world relevance of the results will be evaluated through an implementation in the SAFE-E research project (http://www.safe-project.eu), which aims at developing safe automotive applications that are compliant to ISO 26262 functional safety standard and are based on AUTOSAR architecture.\\

The hardware architectures used in safety critical embedded systems needs high reliability that may be affected due to random hardware failures occurring unpredictably during the lifetime of the electrical system. Such failures are difficult to explain and are often attributed to aging effects~\cite{SAFE_D333b}. Hence, one of the task is to integrate Hardware Reliablilty Metrics identified in ISO 26262 as a new criteria into existing DSE approach of AutoFOCUS3.\\

The current trend of moving towards centralized hardware architectures not only poses a threat to freedom from interference of co-located safety-critical systems, but also to preventing hardware resource violations. It is, therefore, essential to consider various hardware resource constraints associated with the technical architectures such as memory per node, power consumption, cost of hardware architecture, etc. Hence, another task is to integrate various resource constraints as deployment generation criteria into AutoFOCUS3.\\ 

Finally, a simple MOO algorithm will be introduced that optimizes the deployment and schedule generation process by generating an optimal mapping of software components to hardware nodes while respecting the identified safety and resource constraints.    



%\section{Description of the work}
%
%Here I am going to talk about the different phases in which the work was divided. 
%
%1. Finding appropriate safety metrics and resource constraints
%
%2. Integrating them in AF3 with Z3 uses as the SAT solver
%
%3. Integrating and evaluating new DSE optimization methods using a case study 

%
%\section{Limitations}
%
%1. Notation of power - do not differentiate between static and dynamic.
%
%2. Scaling with large industrial use cases is still an issue.

\section{Related Work}

A DSE framework, named FORMULA, has been presented in ~\cite{Kang:2010:AED:2023011.2023014}, which uses SMT solvers to solve a set of global design constraints and search for valid design instances. A generic design space exploration (GDSE) framework for domain independent DSE has been presented in ~\cite{5577992}. Even though these frameworks suggest the usage of SMT solvers for generating feasible solutions, the applicability of SMT solvers for a specific DSE problem (generating deployments and schedules, in our case) in a specific domain (for eg. automotive, aircraft, medical, etc.) has not been talked about in much detail. A framework similar to FORMULA, named DESERT, has been presented in ~\cite{NeemaSztipanovitsKarsai03_ConstraintBasedDesignSpaceExplorationModelSynthesis}. DESERT, however, is introduced for automotive applications, but the aftermath of using various safety and resource constraints hasn't been analyzed in portion. \\     

The applicability of SMT solvers in terms of synthesis of schedules, deployments and even platforms has also been demonstrated in ~\cite{6601578} and ~\cite{6903157}. Furthermore in ~\cite{Schatz:2015:ADE:2744769.2747912}, it was demonstrated how the synthesized solutions, combined with relevant technologies (virtualization, time partitioning, etc.), can guarantee a safety-conformant deployment. We extend this work by explicitly adding the possibility of constraining and optimizing deployments for safety conformance and resource
usage.\\

The safety metrics used in the multi-criteria optimization have their foundations in the ISO 26262 automotive functional safety standard, and investigations into architecture benchmarking in the SAFE research project published in ~\cite{SAFE_D44b}.



\section{Organization of thesis}

The rest of the thesis is organized as follows. Chapter \ref{chapter:theoretical_background} gives a brief introduction of the theories and concepts on which the thesis is based. Chapter \ref{chapter:constraints} introduces various safety and resource constraints identified in ISO 26262 and how they are integrated into the chosen development tool, AutoFOCUS3. A simple MOO algorithm is proposed in chapter \ref{chapter:safeTAG} which is used to generate pareto-optimal deployments. Results of integrating the new constraints into deployment and schedule generation process of AutoFOCUS3 are shown in chapter \ref{chapter:results}. Chapter \ref{chapter:conclusion} concludes the work done in this thesis, while chapter \ref{chapter:assumptions} talks about the various assumptions that we have taken while realizing the work. Chapter \ref{chapter:futureimprovements} talks about the ways in which this work can be improved further.       

%
%\subsection{Subsection}
%See~\autoref{fig:sample}.
%
%\begin{figure}[htsb]
%  \centering
%  \includegraphics{logos/tum}
%  \caption[Example figure]{An example for a figure.}\label{fig:sample}
%\end{figure}
%
%\section{Section}
%
%See~\autoref{tab:sample}, \autoref{fig:sample-drawing}, \autoref{fig:sample-plot}, \autoref{fig:sample-listing}.
%
%\begin{table}[htsb]
%  \caption[Example table]{An example for a simple table.}\label{tab:sample}
%  \centering
%  \begin{tabular}{l l l l}
%    \toprule
%      A & B & C & D \\
%    \midrule
%      1 & 2 & 1 & 2 \\
%      2 & 3 & 2 & 3 \\
%    \bottomrule
%  \end{tabular}
%\end{table}
%
%\begin{figure}[htsb]
%  \centering
%  % This should probably go into a file in figures/
%  \begin{tikzpicture}[node distance=3cm]
%    \node (R0) {$R_1$};
%    \node (R1) [right of=R0] {$R_2$};
%    \node (R2) [below of=R1] {$R_4$};
%    \node (R3) [below of=R0] {$R_3$};
%    \node (R4) [right of=R1] {$R_5$};
%
%    \path[every node]
%      (R0) edge (R1)
%      (R0) edge (R3)
%      (R3) edge (R2)
%      (R2) edge (R1)
%      (R1) edge (R4);
%  \end{tikzpicture}
%  \caption[Example drawing]{An example for a simple drawing.}\label{fig:sample-drawing}
%\end{figure}
%
%\begin{figure}[htsb]
%  \centering
%
%  \pgfplotstableset{col sep=&, row sep=\\}
%  % This should probably go into a file in data/
%  \pgfplotstableread{
%    a & b    \\
%    1 & 1000 \\
%    2 & 1500 \\
%    3 & 1600 \\
%  }\exampleA
%  \pgfplotstableread{
%    a & b    \\
%    1 & 1200 \\
%    2 & 800 \\
%    3 & 1400 \\
%  }\exampleB
%  % This should probably go into a file in figures/
%  \begin{tikzpicture}
%    \begin{axis}[
%        ymin=0,
%        legend style={legend pos=south east},
%        grid,
%        thick,
%        ylabel=Y,
%        xlabel=X
%      ]
%      \addplot table[x=a, y=b]{\exampleA};
%      \addlegendentry{Example A};
%      \addplot table[x=a, y=b]{\exampleB};
%      \addlegendentry{Example B};
%    \end{axis}
%  \end{tikzpicture}
%  \caption[Example plot]{An example for a simple plot.}\label{fig:sample-plot}
%\end{figure}
%
%\begin{figure}[htsb]
%  \centering
%  \begin{tabular}{c}
%  \begin{lstlisting}[language=SQL]
%    SELECT * FROM tbl WHERE tbl.str = "str"
%  \end{lstlisting}
%  \end{tabular}
%  \caption[Example listing]{An example for a source code listing.}\label{fig:sample-listing}
%\end{figure}


