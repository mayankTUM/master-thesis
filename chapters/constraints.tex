\chapter{Phase I: DSE Of Safety Metrics And Resource Constraints}\label{chapter:constraints}

As mentioned in the previous chapter, there are two ways to evaluate hardware architecture against safety. One way is to use \textit{Hardware architectural metrics} and other way is \textit{Evaluation of residual risk of safety goal violation}. We identified two hardware safety metrics in the former category, the metrics being SPFM(single point fault metric) and LFM(Latent fault metric), whereas in the latter category, we discussed about PMHF(probabilistic measure of hardware failures), which describes the overall probability of occurrence of random hardware failures in a hardware component. Since all these metrics define target values for for components marked with specific ASIL levels(table \ref{table:1} and table \ref{table:2}), it makes sense to choose any metric for the purpose of integration into AutoFOCUS3 as the new safety metrics. We have chosen PMHF of the hardware nodes, but the safety metrics in AutoFOCUS3 can be easily extended with SPFM and LFM.   \\

In addition we have included some new resource constraints like maximum number of nodes (ECUs), total cost of HW architecture, total power consumption of HW architecture, memory per node, etc, as the new design criteria while generating the deployments and schedules in AutoFOCUS3.\\

The section 3.1 gives an introduction of these constraints along with their formal specification. Section 3.2 shows the integration of these constraints in AutoFOCUS3. Section 3.3 shows how the Z3 solver can be used to achieve the desired results. 

\section{Safety metrics and deployment constraints}

Ensuring safety of automotive systems against random hardware failures is non-trivial. Such failures are difficult to reason about and can occur unpredictably during the lifetime of the electrical and electronic systems. Accounting for such failures during embedded system engineering is, thus, very important to ensure safety and reliability. We have identified some safety metrics in ISO 26262 that ensure safety against random hardware failures and are, therefore, integrated into the deployment and schedule generation process of AutoFOCUS3. \\

Another important objective is the optimization of cost of hardware architecture onto which the logical components (with given ASIL levels) are deployed. Since each hardware node is marked with a PMHF value, we use the mapping defined in table \ref{table:2} to find the corresponding ASIL level. Different ASIL levels of hardware nodes mean different requirement stringencies and hence different developmental costs~\cite{azevedo2014exploring}. There can be many deployments possible that satisfy safety constraints and resource constraints, but in order to find the most desired and advantageous one, the problem needs to consider different cost implications~\cite{azevedo2014exploring}. \\ 

%Let us explain it with the help of an example. A logical component with a safety goal of level ASIL B can be mapped onto the hardware nodes with either ASIL B, ASIL C or ASIL D. However, the less the PMHF value of a hardware node, the more the developmental cost. It is therefore desirable to map the logical component on an ASIL B node rather than ASIL C or ASIL D node, in order to optimize the cost. We have also integrated the cost constraint into the deployment generation process of AUTOFOCUS3. \\

We have also identified some resource constraints associated with the hardware architecture to make the deployments more reliable. The usage of nodes in hardware architecture is limited by constraints such as memory per node and power consumption. Such constraints are also included in the deployment generation process of AutoFOCUS3. \\    

Each of these constraints are explained in more detail below.

\subsection{PMHF Node Constraint}

The first constraint is the \textit{PMHF node constraint}, wherein a logical component with a given ASIL requirement can be mapped to a hardware node with a PMHF value lying in the range as specified in the table \ref{table:1}. For the ease of representation, we convert the PMHF value of hardware node to the corresponding ASIL value and then check for the compliance of ASIL values of both logical component and hardware node. For e.g. a hardware node with a PMHF value of $< 10^{-7} h^{-1}$ is marked as ASIL B compliant. This means the hardware node is capable of deploying only the components marked with either QM, ASIL A or ASIL B, but not higher ASIL values. The formal specification of PMHF node constraint is as follows. \\


\noindent\fbox{%
	\parbox{\textwidth}{%
Let $T = \lbrace t_0, t_1,...,t_x \rbrace$ be the set of tasks and $N = \lbrace n_0, n_1,...,n_y \rbrace$ be the set of nodes. We define a function $asil$ that takes as input a $task$ or a $node$ and gives its $ASIL$ value as the output, i.e. 
\begin{center}
	$asil: P \rightarrow Q$ ~\\ $\text{where } P = \lbrace T \cup N\rbrace$ ~\\ $\text{and } Q = \lbrace QM, A, B, C, D \rbrace$
\end{center}Given the function $node: T \rightarrow N$, the PMHF constraint ensures that each task needs to be deployed to a node that is at least as reliable as the task itself, where reliability is measured in terms of their ASIL levels. 
\begin{center}  $\forall t \in T', \text{where } T' = \{t' | node(t') = n\}$\\	$asil(t) \leq asil(n)$   \end{center}


	}%
}

\subsection{PMHF Bus Constraint}

The next type of constraint is the PMHF bus constraint, wherein the safety of the communication buses, using which the logical components communicate with each other, is also considered. A reliable communication is ensured when a component receives a message over a bus that is at least as reliable and safe as the receiver itself. The PMHF bus constraint ensures for each message that, if the sending and the receiving component are deployed on the different ECUs, the ASIL of the bus is at least as stringent as the ASIL of the ECU onto which receiving component is deployed. \\ 

\noindent\fbox{%
	\parbox{\textwidth}{%
		 ~\\  
		Let $s \in T$ be the $sending$ task and $r \in T$ be the $receiving$ task. Let $m$ be the message sent from $s$ to $r$ over the communication bus $b \in B$. Given the function $node: T \rightarrow N$, the PMHF bus constraint is defined as: 
		\begin{center}
			$asil(r) \leq asil(b)$ ~\\	$\text{where } node(s) \neq node(r)$	\end{center}  		  
		This constraint needs to hold for all the messages that are exchanged between the logical components.
	}%
}

\subsection{Memory Constraint}

A hardware node can accommodate limited number of logical components, due to the memory constraint it is associated with. Each logical component requires some amount of memory to execute and hence, a hardware node can carry as many logical components whose accumulated memory is no more than the memory available at the hardware node. The formal specification of the memory constraint is given below.\\

\noindent\fbox{%
	\parbox{\textwidth}{%
	Let $n_{ram}$ be the amount of memory available at the node $n$ and $t_{ram}$ be the amount of memory needed by the task $t$ for execution. Given the function $node: T \rightarrow N$, the memory constraint ensures that none of the ECUs run out of memory after accommodating the tasks. This is shown below: 
	\begin{center}
			$\sum\nolimits_{t \in T'} t_{ram} \leq n_{ram}$~\\
		$\text{where } T' = \{t' | node(t') = n\} $	
	\end{center}
	}%
}

\subsection{Node usage Constraint}

Another type of constraint is the node usage constraint, wherein for a given technical architecture consisting of multiple hardware nodes, the deployment generation process can use no more than the number of nodes specified by the user. The formal specification of this constraint is given below. \\

\noindent\fbox{%
	\parbox{\textwidth}{%
	 The node usage constraint ensures that the total number of used nodes is no more than what is entered by the user. 
		\begin{center}
			$total\_used\_nodes \leq max\_nodes$
		\end{center}
		Here, $max\_nodes$ is the maximum number of allowed nodes that the user gives as a constraint for deployment generation. The formula for calculating the total number of used nodes is given below.
		\begin{center}
			$total\_used\_nodes$ = $\sum\nolimits_{n \in N} used(n)$
		\end{center}
		The function $used:N\rightarrow\{0,1\}$ takes as input a $node$ and returns 0 or 1, where 0 indicates that there is no task allocated to a node and 1 indicates that there is atleast one task allocated to the node.
		\begin{center}
			$used(n) =
			\begin{cases}
			1, & \exists t \in T : node(t)=n \\
			0, & \text{otherwise}
			\end{cases}$
		\end{center}
		
	}%
}


\subsection{Cost Constraint}

As mentioned before, each hardware node in the technical architecture is marked with a PMHF value and a corresponding ASIL value. These nodes, marked with different ASIL levels, incurs different developmental costs which converts to discrete time, effort and money. The cost constraint is aimed at reducing the overall cost of the hardware architecture in the sense that the hardware nodes with lower ASIL values are preferred over nodes with higher ASIL values, of course respecting other constraints. The formal specification of the cost constraint is given below. \\ 

\noindent\fbox{%
	\parbox{\textwidth}{%
		Let us define, for each node in $N$, a property named $cost$, which denotes the developmental cost of the hardware node. The total cost of the hardware architecture is calculated as follows.
		 
		\begin{center}
			$total\_cost = \sum\nolimits_{n\in N} n_{cost} * used(n) $
		\end{center}
		
		The definition of the function $used$ is exactly the same as defined in the node usage constraint.\\
		
		 The cost constraint ensures that the total cost of the hardware architecture do not exceed the cost entered by the user. Let the cost entered by the user be $max\_cost$. This constraint, therefore, ensures:
		
		\begin{center}
			$ total\_cost \leq max\_cost $
		\end{center}
	}%
}

\subsection{Energy Consumption Constraint}

We have used a very naïve approach for calculating the total energy consumption of the hardware architecture. Each ECU can be annotated with a power value, indicating the amount of power it consumes in the non-idle state. If the node is in idle state, it is assumed to consume no power or energy at all. Also, we do not complicate the energy consumption calculation by considering static and dynamic power separately. The assumptions, however, may not seem to be quite credible, but, since the sole purpose of using the energy constraint is to minimize the utilization of higher ASIL nodes by minimizing their $non\_idle$ time, these assumptions serve the purpose. \\

\noindent\fbox{%
	\parbox{\textwidth}{%

Given the power consumption of a node i.e. $n_{power}$, the energy consumed by it can be defined as follows: 

\begin{center}
	$n_{energy} = n_{power} * n_{non\_idle\_time} $
\end{center} 

The non-idle time of an ECU is the sum of duration of all tasks allocated to it (eq. 3).

\begin{center}
	$n_{non\_idle\_time} = \sum\nolimits_{t \in T'} t_{duration}$\\
	$\text{where } T' = \{t' | node(t) = n\} $
\end{center} 

The total energy consumed by the hardware architecture is calculated by adding the energy consumed by individual hardware nodes. This is shown below (eq. 4)):

\begin{center}
	$total\_energy\_consumed = \sum\nolimits_{n \in N} n_{energy} $
\end{center} 

The energy consumption constraint ensures that the total energy consumed by the hardware architecture is no more than the amount entered by the user. This is shown as follows:
\begin{center}
	$total\_energy\_consumed \leq   max\_energy  $
\end{center} 

	}%
}~\\

NOTE: The energy consumption calculation can be refined further by taking frequency scaling of individual ECUs into account. This is the process in which the frequency of an ECU can be scaled up or down in order to save power. The frequency scaling of a node i.e. $n_{fs}$ can be directly included in the energy ($n_{energy}$) calculation to give a more refined value. 

\section{Integration into AutoFOCUS3}

This section explains the integration of identified safety and resource constraints into deployment and schedule generation process of AutoFOCUS3. The effects of using such constraints in the deployment generation process are shown using a simple ACC(adaptive cruise control) system which is modeled in AutoFOCUS3. The two different layers of abstraction of this system, namely, logical architecture and technical architecture, are shown in figure \ref{la_acc} and figure \ref{ta_acc} respectively. \\

\begin{figure}[ht!]
	\centering
	\includegraphics[width=150mm]{figures/acc_la.png}
	\caption{Logical architecture of ACC system \label{la_acc}}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=150mm]{figures/acc_ta.png}
	\caption{Technical architecture of ACC system \label{ta_acc}}
\end{figure}

\subsection{Annotations}

In AutoFOCUS3, there is an \textit{annotation view} for each layer of abstraction. The annotation view for logical architecture contains properties associated with logical components. Similarly, the annotation view for hardware architecture contains properties associated with the hardware components. The annotation view for logical architecture is shown in the figure \ref{acc_la_annot}. \\ 

\begin{figure}[ht!]
	\centering
	\includegraphics[width=150mm]{figures/acc_la_annot.png}
	\caption{Annotation view for logical architecture of ACC system \label{acc_la_annot}}
\end{figure}

To implement \textit{PMHF node constraint} and \textit{PMHF bus constraint}, we need to define a safety level for each logical component. This annotation is shown in the blue box of figure \ref{acc_la_annot}. There are three possible standards that we can use to define the safety levels of logical components; ISO 26262, IEC 61508 and DO 178C. However we just need to concentrate on ISO 26262, which limits the safety level annotation to five values; QM, ASIL-A, ASIL-B, ASIL-C and ASIL-D. \\ 

Memory per node constraint requires that the sum of memories of the logical components assigned to a node should not exceed the memory of the node itself. This requires us to define memory for each logical component in the logical architecture. The annotation \textit{Memory:local}, shown in the red box, is used for this purpose. \\

As mentioned in the previous section, to implement power constraint, we need to find out the total non idle time of each hardware node. The non idle time of a node is calculated by accumulating the duration of all the logical components or tasks allocated to the node. This requires us to define another annotation for logical components i.e. \textit{Timing}, which is shown in the green box. \\

\begin{figure}[ht!]
	\centering
	\includegraphics[width=150mm]{figures/acc_ta_annot.png}
	\caption{Annotation view for technical architecture of ACC system \label{acc_ta_annot}}
\end{figure}

The annotation view for the technical architecture of ACC system is shown in figure \ref{acc_ta_annot}. Since, a logical component annotated with a safety level, should be mapped to a hardware node with a specific PMHF value, we define an annotation for hardware nodes, named, PMHF. Also, a reliable communication between two logical components is decided based on the reliability of the communication bus itself. Hence, PMHF annotation is applicable for the communication bus as well. This annotation is shown in the blue box of figure \ref{acc_ta_annot}. \\

For implementing the memory per node constraint, in addition to defining memory annotation for logical components, we need to define memory annotation for hardware nodes as well. This is shown in the red box. \\

Each hardware node consumes some amount of power when running. To calculate the total energy consumed by the hardware architecture, we need to define a power label for each hardware node. This power is not separated into static and dynamic power consumption, since we assume to take only the non-idle time of the node while calculation the energy consumption. This annotation is shown in the green box. \\

Similarly, each hardware node has some associated operational cost. Nodes with less PMHF value (or more reliability from random hardware failures) may include complex circuitry and functionality and hence more developmental efforts. This converts to time, effort and money. Therefore, we define a cost annotation for each hardware node, which is shown in the yellow box of figure \ref{acc_ta_annot}.\\   

\subsection{Aftermath of using new constraints on ACC model} 

The deployment and schedule generation process of AutoFOCUS3 is extended with the identified safety metrics and resource constraints, as shown in the figure \ref{constraints_integration}. Assuming we take the annotation values as shown in figure \ref{acc_la_annot} and in figure \ref{acc_ta_annot}, we then depict how each of these constraints affect the deployment generation process, starting with \textit{PMHF node constraint}. The values of different annotations are listed in table \ref{table:3} and table \ref{table:4} for logical architecture and technical architecture respectively.\\


\begin{figure}[ht!]
	\centering
	\includegraphics[width=150mm]{figures/constraints_integration.png}
	\caption{Constraints integration into AUTOFOCUS3 \label{constraints_integration}}
\end{figure}

\begin{table}[h!]
	\centering
	\def\arraystretch{1.5}
	\begin{tabular}{ | l | l | l | l |}
		\hline
		Logical component & Local Memory & Safety Level & Duration \\ \hline
		AccelerationControl & 10 & ASIL A & 10 \\ \hline  
		DistanceControl & 10 & ASIL B & 10 \\ \hline
		DistancePlausibilization & 10 & ASIL C & 10 \\ \hline
		SpeedControl & 10 & ASIL A & 10 \\ \hline
		SpeedPlausibilization & 10 &  ASIL C & 10 \\ \hline
	\end{tabular}
	\caption{Annotation values for logical architecture of ACC}
	\label{table:3}
\end{table}


\begin{table}[h!]
	\centering
	\def\arraystretch{1.5}
	\begin{tabular}{ | l | l | l | l | l |}
		\hline
		Hardware Component & Memory per node & PMHF & Power Consumption & Cost \\ \hline
		HeadUnitECU & 30 & 0.3 & 20 & 8 \\ \hline  
		InterdomainFlexRay & N/A & 0.8 & N/A & N/A \\ \hline
		PowertrainECU & 30 & 0.6 & 10 & 5 \\ \hline
	\end{tabular}
	\caption{Annotation values for technical architecture of ACC}
	\label{table:4}
\end{table}

\textbf{Constraint 1: PMHF node constraint}

PMHF node constraint restricts the deployment generation process by allowing the logical components, with specific ASIL values, to be mapped only to safety compatible hardware nodes, which is decided by their PMHF values. Assuming that only PMHF node constraint is applied to the deployment generation process and assuming that the annotation values are taken from table \ref{table:3} and table \ref{table:4}, the following mapping of logical components is possible, shown in \ref{table:5}.\\

\begin{table}[h!]
	\centering
	\def\arraystretch{1.5}
	\begin{tabular}{ | l | l |}
		\hline
		Logical component & Hardware Nodes  \\ \hline
		AccelerationControl &  HeadUnitECU, PowertrainECU \\ \hline  
		DistanceControl &  HeadUnitECU, PowertrainECU \\ \hline
		DistancePlausibilization &  HeadUnitECU \\ \hline
		SpeedControl &  HeadUnitECU, PowertrainECU \\ \hline
		SpeedPlausibilization &  HeadUnitECU \\ \hline
	\end{tabular}
	\caption{Effect of PMHF node constraint}
	\label{table:5}
\end{table}

Since the logical components \textit{DistancePlausibilization} and \textit{SpeedPlausibilization} are labeled as ASIL C components, the only hardware node they can be mapped onto is HeadUnitECU (which is ASIL C compatible), whereas the rest of the components can be mapped onto any hardware node. NOTE: these are the possible deployments when only PMHF node constraint is applied.\\

\textbf{Constraint 2: PMHF bus constraint} \\

PMHF bus constraint ensures reliable communication between the logical components in the sense that the receiving component can receive a message only over a communication bus having a PMHF value compatible to the ASIL level of that component. As indicated in the table \ref{table:6}, the logical component \textit{DistanceControl}, which is ASIL B compliant, cannot receive a message over the \textit{InterdomainFlexRay} bus, which is ASIL A compliant, thereby, forcing the logical components \textit{SpeedPlausibilization}, \textit{DistancePlausibilization} and \textit{DistanceControl} to be deployed onto the same hardware node i.e. \textit{HeadunitECU}. \\

\begin{table}[h!]
	\centering
	\def\arraystretch{1.5}
	\begin{tabular}{ | l | l |}
		\hline
		Logical component & Hardware Nodes  \\ \hline
		AccelerationControl & HeadUnitECU, PowertrainECU \\ \hline  
		DistanceControl &  HeadUnitECU \\ \hline
		DistancePlausibilization &  HeadUnitECU \\ \hline
		SpeedControl &  HeadUnitECU, PowertrainECU \\ \hline
		SpeedPlausibilization &  HeadUnitECU \\ \hline
	\end{tabular}
	\caption{Effect of PMHF node constraint and PMHF bus constraint}
	\label{table:6}
\end{table}

\textbf{Constraint 3: Memory per node constraint}\\

The memory of each hardware node, as shown in table \ref{table:4}, is 30 units. Assuming that \textit{PMHF node constraint}, \textit{PMHF bus constraint} and \textit{memory per node constraint} holds, the deployment gets more restricted in the sense that the hardware node \textit{HeadUnitECU} can accommodate at most 3 logical components (since each logical component requires 10 units of memory). The only possible deployment when these three constraints are applied is shown in table \ref{table:7} below. \\


\begin{table}[h!]
	\centering
	\def\arraystretch{1.5}
	\begin{tabular}{ | l | l |}
		\hline
		Logical component & Hardware Nodes  \\ \hline
		AccelerationControl & PowertrainECU \\ \hline  
		DistanceControl &  HeadUnitECU \\ \hline
		DistancePlausibilization &  HeadUnitECU \\ \hline
		SpeedControl &  PowertrainECU \\ \hline
		SpeedPlausibilization &  HeadUnitECU \\ \hline
	\end{tabular}
	\caption{Effect of PMHF node constraint, PMHF bus constraint and Memory constraint}
	\label{table:7}
\end{table}

\textbf{Constraint 4: Node usage constraint}\\

Assuming the node usage constraint is applied, in addition to the previous 3 constraints, the only possible deployment is the one that uses atleast 2 hardware nodes, \textit{HeadUnitECU} and \textit{PowertrainECU} in this case. So the deployment remains the same as shown in table \ref{table:7}.\\

\textbf{Constraint 5: Energy/Power consumption constraint}\\

As shown in the table \ref{table:7}, the hardware node \textit{HeadUnitECU} accommodates 3 logical components and the hardware node \textit{PowertrainECU} accommodates 2 logical components. Since each logical component runs for 10 units of time, the total non-idle time of \textit{HeadUnitECU} and \textit{PowertrainECU} is 30 and 20 units respectively. Since we follow a naïve approach for calculating the energy consumption, where in we consider only the non-idle time of ECU, the total energy consumed by the technical architecture cannot be less than 800 units, where \textit{HeadUnitECU} consumes 600 units(30*20) and \textit{PowertrainECU} consumes 200 units(20*10) of energy.\\

\textbf{Constraint 6: Cost constraint}\\

Similar to the way in which energy consumption is calculated, the cost of the hardware architecture needs to be atleast 13 (8 for \textit{HeadUnitECU} and 5 for \textit{PowertrainECU}). If the user enters a cost less than 13, there would be no deployments possible.\\

So, this section has briefly explained how the different constraints can help in yielding deployments that are safe and reliable in context of random hardware failures and that do not violate the resource constraints. We have used a simple ACC example for explaining how these constraints work, but in the coming chapters, we will use an industrial-like case study containing large number of software and hardware components.   \\ 


\section{Use of SMT solvers to generate deployments}

In this section, our objective is to demonstrate the use of state-of-the-art SMT solvers (Z3 in our case) to generate deployments respecting various safety metrics and resource constraints. Our work is an extension of the work shown in ~\cite{6601578} and ~\cite{6903157}. The Z3 syntax of these constraints is shown below.\\ 

\textbf{Definitions:} The precedence graph used in the deployment and scheduling model of AutoFOCUS3 (see section \ref{subsec: Dep&Sched}) comprises of several entities; a set of tasks $(T)$, a set of nodes $(N)$ and a set of buses $(B)$. Hence, we begin by first defining these entities. \\

\textit{Definition 1 (Tasks)}: To define a new type specification, we use \texttt{declare-sort} command. This command creates a new sort symbol that is an abbreviation for a sort expression. The properties defined for tasks are as follows: 

\texttt{\\
	(declare-sort TASKS) \\	
	(declare-fun t () TASKS) \\
	(declare-fun t\_start () Int) \\
	(declare-fun t\_end () Int) \\
	(declare-fun t\_duration () Int) \\
	(declare-fun t\_sil () Int) \\
	(declare-fun t\_ram () Int) \\
	(declare-fun t\_node () NODES) \\
	}  

The variable $t\_start$ denotes the starting, $t\_duration$ denotes the computation duration and $t\_end$ denotes the finishing time of a certain task. The variable $t\_node$ represents the core, on the mult-core chip, the task \textit{t} is allocated to. In addition, each task has a dedicated ASIL level, denoted by variable $t\_sil$ and needs a certain amount of memory, denoted by variable $t\_ram$. \\

\textit{Definition 2 (Nodes)}: The properties for the core (or node) type specification is defined as follows:

\texttt{\\
	(declare-sort NODES) \\
	(declare-fun n () NODES) \\
	(declare-fun n\_sil () Int) \\
	(declare-fun n\_ram () Int) \\
	(declare-fun n\_power () Int) \\
	(declare-fun n\_cost () Int) \\
	(declare-fun n\_consumed\_memory () Int) \\
	(declare-fun n\_used () Int) \\
	}

The variable $n\_sil$ represents the ASIL level of the node, which is decided based on its PMHF value. Each hardware node has a memory, a power label and a cost label associated with it, denoted by the variables $n\_ram$, $n\_power$ and $n\_cost$ respectively. In addition, we have two helping variables namely, $n\_consumed\_memory$ and $n\_used$, where the former stores the total amount of memory consumed by all the tasks allocated to the node $n$ and the latter indicates whether the node $n$ has at least one task allocated to it i.e. whether it is used in the hardware architecture or not. \\
	

\textit{Definition 3 (Buses)} : The third kind of type specification is for the communication bus, which is used to carry the messages transferred between logical components. The properties defined for a communication bus are as follows:   
	
\texttt{\\
	(declare-sort BUSES) \\
	(declare-fun b () BUSES) \\
	(declare-fun b\_sil () Int) \\
	}	

The only variable defined in the $BUSES$ type specification is $b\_sil$, which defines the safety level of the communication bus. This variable is needed to implement the \textit{PMHF bus constraint}.\\

\textbf{Functions:} In this section, we define some helping functions needed to implement the constraints. These are listed below:\\

\textit{Function 1 (Getter functions)}: We have defined four simple getter functions namely, $getNode$, $getRAM$, $getDuration$ and $getUsed$. The function $getNode$ returns the node onto which a specific task is deployed. The function $getRAM$ returns the amount of memory needed by the task to execute. The function $getDuration$ returns the amount of time for which a task runs. Finally, the function $getUsed$ returns a 1 or 0 indicating whether a node is used or not. The Z3 syntax of these functions is as follows:

\texttt{\\
	(declare-fun getNode (TASKS) NODES) \\
	(declare-fun getRAM (TASKS) Int) \\
	(declare-fun getDuration (TASKS) Int) \\
	(declare-fun getUsed (NODES) Int) \\
	}

These functions need the following set of assertions to complete their definition. The first three assertions are  defined for all the tasks and the last assertion is defined for all the nodes.

\texttt{\\
	(assert (= t\_node (getNode t))) \\
	(assert (= t\_ram (getRAM t))) \\
	(assert (= t\_duration (getDuration t))) \\
	(assert (= n\_used (getUsed n))) \\
}

\textit{Function 2 (checkUsageRAM)} : This function takes as input a task and a node and returns the amount of memory the task consumes on the node. The function is shown below:

\texttt{\\
(define-fun checkUsageRAM ((t TASKS)(n NODES)) Int (ite (= (getNode t) n) (getRAM t) 0)) \\
}

\textit{Function 3 (checkDuration)} : This function takes as input a task and a node and returns the amount of time the task contributes to the node's non idle time. The function is shown below:

\texttt{\\
	(define-fun checkDuration ((t TASKS)(n NODES)) Int (ite (= (getNode t) n) (getDuration t) 0)) \\
}


\textbf{Assertions:} In this section, we define the assertions used to implement the identified safety metrics and resource constraints. \\

\textit{Assertion 1 (Task attributes)}: Each task $t$ has a certain computation time $(d_t)$, a safety integrity level $(s_t)$ and an amount of memory $(r_t)$ it consumes. The following set of assertions assign certain values to these variables. 

\texttt{\\
	(assert (= t\_duration $d_t$)) \\
	(assert (= t\_sil $s_t$)) \\
	(assert (= t\_ram $r_t$)) \\
	}

\textit{Assertion 2 (Node attributes)} : Each node $n$ has a safety integrity level $(s_n)$, an amount of available memory $(r_n)$, a power label $(p_n)$ and a cost label $(c_n)$ associated with it. The following set of assertions assign certain values to these variables. 

\texttt{\\
	(assert (= n\_sil $s_n$)) \\
	(assert (= n\_ram $r_n$)) \\
	(assert (= n\_power $p_n$)) \\
	(assert (= n\_cost $c_n$)) \\
	}

\textit{Assertion 3 (Bus attributes)}: The $BUSES$ type specification has only one variable ($b\_sil$) that needs to be asserted. Let $s_b$ be the safety integrity level of bus $b$. The assertion for the variable is as follows: 

\texttt{\\
	(assert (= b\_sil $s_b$)) \\
}

\textit{Assertion 4 (PMHF node constraint)}: For all task $t\in T$ and all nodes in $n \in N$, the PMHF node constraint ensures:

\texttt{\\
	(assert (! (ite (= t\_node n) (ite (<= t\_sil n\_sil) true false) true))) \\
	}


\textit{Assertion 5 (PMHF bus constraint)}:For every pair of communicating logical components deployed on separate nodes, the bus needs to be at least as safe (in terms of safety level) as the receiving component. If $t_i \in T$ and $t_j \in T$ represents the sending and receiving tasks respectively and $b \in B$ denotes the bus, the assertion for \textit{PMHF bus constraint} is defined as follows: 

\texttt{\\
	(assert (! (=> (not (= $t_i\_node$ $t_j\_node$)) (<= $t_j\_sil$ b\_sil)))) \\
	}

\textit{Assertion 6 (Memory per node constraint)}: Given a set of tasks $T = \lbrace t_0, t_1, ... t_x\rbrace$, the total memory consumed at a node $n$ is calculated as follows:

\texttt{\\
	(assert (= n\_consumed\_memory (+ (checkUsageRam $t_0$ n) (+ (checkUsageRam $t_1$ n) (+ ... (checkUsageRam $t_x$ n))))))\\
	}

The \textit{memory per node} constraint ensures that the memory consumed at a node should not exceed the memory available at that node. This is asserted as follows:

\texttt{\\
	(assert ( (<= n\_consumed\_memory n\_ram)))\\
	} 
	
\textit{Assertion 7 (Node usage constraint)}: Depending on whether the consumed memory ($n\_used\_memory$) of a node $n$ is greater than 0 or not, we can decide its usage. The assertion to do that is as follows:

\texttt{\\
	(assert (= n\_used (ite (not (= n\_consumed\_memory 0)) true false)))\\
} 

We define an additional function that converts the usage of a node from a boolean value to an integer value of either 0 or 1. This will be helpful in calculating total number of used nodes. The function is defined as follows:

\texttt{\\
	(define-fun boolToIntUsed ((n NODES)) Int (ite (= (getUsed n) true) 1 0))\\
} 

Given a set of nodes $N=\{n_0,n_1, ...,n_y\}$, the assertion to calculate the total used nodes is defined as follows:

\texttt{\\
	(declare-fun total\_used\_nodes () Int)\\
	(assert (= total\_used\_nodes (+ (boolToIntUsed $n_0$) (+ (boolToIntUsed $n_1$) (+ ... (boolToIntUsed $n_y$))))))\\
} 

The final assertion is to make sure that the total number of used nodes do not exceed the number entered by the user. Given $max\_nodes$ as the maximum number of nodes allowed for usage, the \textit{node usage constraint} is defined as follows:

\texttt{\\
	(assert (<= total\_used\_nodes max\_nodes))\\
} 

\textit{Assertion 8 (Cost constraint)}: Given a $cost$ attribute for each node in $N = \{n_0, n_1, ...,n_y \}$, the total cost of the hardware architecture can be calculated as follows:

\texttt{\\
	(declare-fun total\_cost () Int)\\
	(assert (= total\_cost  (+ (* $n_0\_used$ $n_0\_cost$) ( + (* $n_1\_used$ $n_1\_cost$) (+ ... (* $n_y\_used$ $n_y\_cost$))))))\\
} 
 	
The total cost of the hardware architecture needs to be less than or equal to the cost entered by the user, which is ensured by the following assertion:

\texttt{\\
	(assert (<= total\_cost max\_cost))\\
} 

\textit{Assertion 9(Energy consumption constraint)}: Given a set of tasks, $T = \{t_0, t_1, ..., t_x\}$, the non idle time of a node $n$ is calculated as follows:

\texttt{\\
	(assert (= n\_nonidle\_time (+ (checkDuration $t_0$ n) (+ (checkDuration $t_1$ n) (+ ... (checkDuration $t_x$ n))))))\\
} 
 
After finding the non idle time of each node in $N=\{n_0, n_1, ..., n_y\}$, the total energy consumption of the hardware architecture can be calculated as follows:

\texttt{\\
	(declare-fun total\_energy\_consumption () Int)\\
	(assert (= total\_energy\_consumption (+ (* $n_0\_power$ $n_0\_nonidle\_time$) (* $n_1\_power$ $n_1\_nonidle\_time$) (+ ... (* $n_y\_power$ $n_y\_nonidle\_time$))))))\\
}   	

The total energy consumed by the hardware architecture must not exceed the value entered by the user. This is asserted as follows:

\texttt{\\
	(assert (<= total\_energy\_consumption max\_energy))\\
} 
 	
\section{UnSAT Cores}

In the previous section we showed how the state-of-the-art SMT solvers can be used for deciding the satisfiability of a deployment and schedule generation problem. If a constraint satisfaction problem in not satisfiable, it is sometimes difficult to manually find the constraints that are not satisfied, for e.g. in a system consisting of hundreds of logical components and tens of hardware components. \\

 Solvers may be designed to not only be capable of determining whether a formula is satisfiable or not, but also to point ot the set of clauses which are not satisfiable, called unsatisfiable cores or UnSAT cores. For e.g. if the memory of a hardware node, say $core1$, is exceeded, the user will be prompted with a message as shown in figure \ref{unsat}. One option to resolve this unsatisfiability clause is to increase the memory of $core1$. The respective messages are displayed for each type of constraint in case it leads to an unsatisfiable clause.
 
 \begin{figure}[ht!]
 	\centering
 	\includegraphics[width=150mm]{figures/unsat.png}
 	\caption{UnSAT core in case of memory constraint violation \label{unsat}}
 \end{figure}
 