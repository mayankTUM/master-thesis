\chapter{Phase II: Pareto-Optimal Deployment Generation}\label{chapter:safeTAG}

In the previous chapter, we presented a formalization of the identified safety metric and resource constraints and showed how they can be realized using Z3 SMT solver. Till now, we have focused on generating only a valid deployment that satisfies these constraints. But in practice, a large system can have multiple deployments that are considered valid, in the sense that all of them satisfies the identified constraints. Hence, it makes sense to reduce the design space even further by optimizing the certain objectives and focusing only on the non-dominated set of deployments. These set of deployments that do not dominate each other, forms the pareto-optimal set. The criteria for optimization are as follows:

\begin{enumerate}
	\item Total number of nodes. (Integer)
	\item End-to-end latency. (Integer)
	\item Memory per node. (Integer)
	\item Total SIL of the hardware architecture. (Integer, QM $\rightarrow$ 0, ASIL\_A $\rightarrow$ 1, ASIL\_B $\rightarrow$ 2, ASIL\_C $\rightarrow$ 3, ASIL\_D $\rightarrow$ 4)
\end{enumerate}

The first criteria i.e. \textit{total number of nodes}, is directed towards minimizing the node usage and the second criteria i.e. \textit{end-to-end latency}, is directed towards minimizing the runtime of the generated schedule including the bus communication time. The third criteria i.e. \textit{memory per node} is directed towards optimizing the amount of memory needed by the hardware nodes to accommodate the logical components. However, to keep the algorithm simple, we assume that each hardware node is allocated the same amount of memory. The fourth criteria needs a bit of explanation. Higher safety requirements of a hardware node comes with the cost of more developmental effort and implementing a functionality that consumes more power. Since the \textit{cost} and the \textit{power consumption} of the hardware architecture depends on the safety level or the SILs of the hardware nodes, we try to optimize the total SIL of the hardware architecture rather than optimizing the resources separately. \\

The problem of generating the pareto optimal deployments can be formulated in simple terms as follows:

\begin{center}
	\textbf{Minimize} \\
	$number\_of\_nodes$ \\
	$memory\_per\_node$ \\
	$total\_sil$ \\
	$e2e\_latency$ \\
	\textbf{such that} \\
	$number\_of\_nodes \leq max\_nodes$ \\
	$memory\_per\_node \leq max\_memory$ \\
	$total\_sil \leq upper\_bound\_sil$ \\
	\textit{PMHF constraint holds} \\
\end{center}	

The values of variables $max\_nodes$, $max\_memory$ and $upper\_bound\_sil$ are entered by the user, if they choose to generate pareto-optimal deployments. The PMHF constraint are also needed to be satisfied while generating the pareto-optimal deployments. Since we are aiming at minimizing the total SIL of the hardware architecture, we do not use \textit{cost constraint} and \textit{energy consumption constraint} explicitly. \\

 	

Section 4.1 describes a very simple MOO algorithm that we have used to generate pareto-optmial deployments and section 4.2 shows the integration of these algorithms into AutoFOCUS3.\\

  
\section{A new MOO Algorithms}

This section explains a simple MOO algorithm which is divided into two parts. The first part aims at generating the set of all valid deployments, whereas, the second part eliminates the deployments that gets dominated by others and generates a set of pareto-optimal deployments. \\

As with any other MOO algorithm, we have to define the decision variable space which can then be converted to the objective function space. Now the question arises: "How to decide the decision variables?". Before answering the question, let's discuss a shortcoming associated with the SMT solvers, since we are using them to decide the satisfiability of constraints. The problem with most of the SMT solvers is that they do not handle the quantifiers well and may take a lot of time to decide the satisfiability of constraints for a large system. Hence we must find a way to reduce the decision variable space as much as possible. By choosing decision variables as \textit{number\_of\_nodes} and \textit{memory\_per\_node}, we can reduce the decision variable space by eliminating the \textit{obviously unsatisfiable} instances. This is illustrated as follows:\\

Consider a scenario wherein the logical architecture contains 5 components and each component requires 10 units of memory. Let the values of variables \textit{max\_nodes} and \textit{max\_memory} be 4 and 20 respectively. It is clear that there will be no possible solution if the number of nodes in the technical architecture is less than 3, since the total memory needed to accommodate the 5 logical components is 50 and maximum memory allowed per node is 20. Similarly, the memory per node can be no less than 20 (assuming memory per node is a multiple of 10). Hence, we define two new variables \textit{min\_nodes} and \textit{min\_memory}, whose values are calculated using the algorithm \ref{alg:lowerbounds}.
 

 
 \begin{algorithm}
 	\caption{Lower Bounds}\label{alg:lowerbounds}
 	\begin{algorithmic}[1]
 		\Procedure{FindLowerBounds}{}
		\State $total\_consumed\_memory \gets 0$
		\State $min\_nodes \gets 0$
		\State $min\_memory \gets 0$
 		\For  {i in 1 .. n}
 		\State $total\_consumed\_memory \gets total\_consumed\_memory + t_i\_{mem}$
 		\EndFor
 		\While {$min\_nodes * max\_memory < total\_consumed\_memory$} all
 		\State $min\_nodes \gets min\_nodes + 1$
		\EndWhile
 		\While {$min\_memory * max\_nodes < total\_consumed\_memory$} 
 		\State $min\_memory \gets min\_memory + 10$
		\EndWhile
 		\EndProcedure
 	\end{algorithmic}
 \end{algorithm}
 
Lines 5-6 adds the memory of each logical component ($n$ is the number of tasks) and stores it in variable $total\_consumed\_memory$, which is then used to calculate minimum number of nodes and minimum memory per node, as shown in lines 7-8 and lines 9-10, respectively.\\ 

The next step is to generate a set of valid deployments. For each pair of \textit{nodes} and \textit{memory}, where \textit{nodes} are in the range [min\_nodes, max\_nodes] and \textit{memory} is in the range [min\_memory, max\_memory], we create a constraint satisfaction problem, which is solved using the Z3 solver. The \texttt{checkSAT(nodes, memory, sil)} function shown in the algorithm \ref{alg:validsolutions} creates a SMT file containing several assertions. This file is then passed to the SMT solver (Z3) which decides the satisfiability of the constraints. The parameters \textit{nodes}, \textit{memory} and \textit{sil} in the $checkSAT()$ function, forms the values of variables $max\_nodes$, $max\_memory$ and $upper\_bound\_sil$ respectively (in the MOO problem defined before).  The new assertions added to this problem are as follows: \\

\textbf{Assertion 1: Assigning SIL}: Instead of assigning the SIL levels to the hardware nodes and bus, we let the Z3 solver choose an appropriate SIL level for each hardware component while keeping the \textit{PMHF node constraints} and \textit{PMHF bus constraints} satisfied. The assertions for letting the Z3 solver decide the SIL level are as follows:

\texttt{\\
	(assert (and (<= 0 n\_sil) (<= n\_sil max\_sil)))
	}

\texttt{\\
	(assert (and (<= 0 b\_sil) (<= b\_sil max\_sil))) \\
}
	
Here, $max\_sil$ is the maximum SIL assigned to any logical component. \\

\textbf{Assertion 2: Assigning memory}: Similar to the previous assertion, we let the Z3 solver decide the appropriate value of memory for each node. The assertion is as follows:

\texttt{\\
	(declare-fun maxMemoryPerNode () Int)\\
	(assert (= n\_ram maxMemoryPerNode)) \\
}

\textbf{Assertion 3: Total SIL constraint}: We now define an assertion that ensures the total sil of the hardware nodes do not exceed a given value. Given the set of nodes, $N = \lbrace n_0, n_1, ..., n_x\rbrace$,  the assertions assuring this are as follows.

\texttt{\\
	(declare-fun totalAllocatedSIL () Int) \\
	(assert (= totalAllocatedSIL (+ $n_0\_sil$ (+ $n_1\_sil$ .... (+ ... ($n_x\_sil$)))))) \\
	(assert (<= totalAllocatedSIL some\_sil)) \\
	}   

Here the value of the variable $some\_sil$ is decided by the algorithm, which is introduced in the next section. \\

\textbf{Assertion 4: Memory per node constraint}: The constraint to assure that the memory per hardware node do not exceed a certain value is given below:

\texttt{\\
	(assert (<= maxMemoryPerNode some\_memory)) \\
}   

Here the value of the variable $some\_memory$ is decided by the algorithm, which is introduced in the next section. \\

The \textit{PMHF node constraint}, \textit{PMHF bus constraint} and \textit{node usage constraint} remains the same as explained in the previous chapter. We now show how these new assertions comes into play while generating a set of valid deployments. This is shown in the algorithm \ref{alg:validsolutions}. 


\begin{algorithm}
	\caption{Valid Solutions}\label{alg:validsolutions}
	\begin{algorithmic}[1]
		\Procedure{ValidSolutions}{}
%		\BState $\forall pair(nodes, memory) \in S$
		\For {$nodes$ in $min\_nodes$ .. $max\_nodes$}
		\For {$memory$ in $min\_memory$ .. $max\_memory$}
		\State $result \gets CheckSAT(nodes, memory, upper\_bound\_sil)$
		\If {\text{$result$ is $SAT$}}
		\State $solution \gets parse(result)$
		\State \text{store $solution$ to $UniqueSolutions$}
		\State $total\_sil \gets extractSIL(solution)$
		\While {\text{$result$ is SAT}}
		\State $total\_sil \gets total\_sil - 1$
		\State $result \gets CheckSAT(nodes, memory, total\_sil)$
		\If {\text{$result$ is $SAT$}}
		\State $solution \gets parse(result)$
		\State \text{store $solution$ to $UniqueSolutions$}
		\EndIf
%		\EndIf
		\EndWhile
%		\EndIf
		\EndIf
		\EndFor
		\EndFor
		\EndProcedure
	\end{algorithmic}
\end{algorithm}


Lines 2-4 shows that for each pair of nodes and memory in decision variable space, a constraint satisfaction problem is created which is solved by the Z3 SMT solver. The $checkSAT()$ function formulates the problem in Z3 syntax, where the parameters $nodes$, $memory$ and $upper\_bound\_sil$ defines the upper limits for assertions used in node usage, memory per node and total sil constraints, respectively . Line 5 checks if the constraint satisfaction problem is satisfiable. In case it is satisfiable, the output of the Z3 solver is parsed to fetch \textit{number\_of\_nodes}, \textit{memory\_per\_node}, \textit{total\_sil} and \textit{e2e\_latency}. These variables are combined into a $solution$ (line 6) and stored into a set of unique solutions (line 7), as we do not want to duplicate the set of valid deployments. We then try to optimize the \textit{total\_sil} by reducing it in steps of 1 (line 10) and check the satisfiability of new constraint satisfaction problem (line 11). The reduction of \textit{total\_sil} continues until Z3 cannot find a solution to the constraint satisfaction problem (line 9). \\

To keep the algorithm simple, we do not try to improve end-to-end latency as it further constrains the problem, making it difficult to find valid deployments (in limited time) for large industrial use-cases. We, therefore, use end-to-end latency only as one of the pareto-optimization criteria to generate pareto-optimal deployments from set of valid deployments. The set of solutions stored in \textit{UniqueSolutions} are then reduced further, since we are interested only in set of solutions that do not dominate each other. We have used a method from ~\cite{deb2001multi} to generate non-dominated or pareto-optimal deployments. It is shown below:

\begin{enumerate}
	\item Initialize $ParetoFront$ with first member of $UniqueSolutions$ i.e. $ParetoFront = UniqueSolutions[1]$. Set solution counter $i = 2$.
	\item Set $j = 1$.
	\item Compare $UniqueSolutions[i]$ with $ParetoFront[j]$ for domination.
	\item If $UniqueSolutions[i]$ dominates $ParetoFront[j]$, delete the $j-th$ member from $ParetoFront$ i.e. update $ParetoFront = ParetoFront - ParetoFront[j]$. If $j < |ParetoFront|$, increment $j$ by one and then go to Step 3. Otherwise, go to Step 5. Alternatively, if the $ParetoFront[j]$ dominates $UniqueSolutions[i]$, increment $i$ by one and then go to step 2.
	\item Insert $UniqueSolutions[i]$ in $ParetoFront$ i.e. $ParetoFront = ParetoFront \bigcup UniqueSolutions[i]$. If $i < N$, increment $i$ by one and go to Step 2. Otherwise, stop and declare $ParetoFront$ as the non-dominated set.
\end{enumerate}       

Here $N$ is the total number of valid deployments. The domination between two different solutions is decided based on four criteria: $nodes$, $memory$, $sil$ and $e2eLatecny$. Given two solutions i and j, solution i dominates solution j iff. 

\texttt{\\
	i.nodes <= j.nodes and i.memory <= j.memory and i.sil <= j.sil and i.e2elatency <= j.e2elatency\\
	}

The \textit{less than or equal} relation should hold for all the criteria, since we need to avoid duplicate solutions having same value for all the constraints. \\

This explains a very simple MOO algorithm which we have used to generate pareto-deployments. The next section shows the integration of this algorithm into AutoFOCUS3 and demonstrates the generation of pareto-optimal deployments for a simple ACC system model.  	
%    
%
%\begin{algorithm}
%	\caption{Finding Pareto Front}\label{alg:pareto}
%	\begin{algorithmic}[1]
%		\Procedure{ParetoFront}{}
%		\State $ParetoFront \gets UniqueSolutions[1]$
%		\State $index1 \gets 2$
%		\State $index2 \gets 1$ 
%		\While {$index1 \leq length(UniqueSolutions)$}
%		\State $X \gets WhoDominates(ParetoFront[index2], UniqueSolutions[index1])$
%		\If {$X = 2$}
%		\State $PF \gets PF\setminus UniqueSolutions[index2]$
%		\EndIf
%		\If {$X = 2 \lor X = 0$}
%		\If {$index2 < length(ParetoFront)$}
%		\State $index2 = index2 + 1$
%		\Else
%		\State \text{add $UniqueSolutions[index1]$ to $ParetoFront$}
%		\If {$index1 < length(UniqueSolutions)$}
%		\State $index1 \gets index1 + 1$
%		\State $index2 \gets 0$
%		\EndIf
%		\EndIf
%		\If {$X = 1$}
%		\State $index1 \gets index1 + 1$
%		\State $index2 \gets 0$
%		\EndIf
%		\EndIf
%		\EndWhile
%		\EndProcedure
%	\end{algorithmic}
%\end{algorithm}
%
%

\section{Integration into AutoFOCUS3}

We use a simple ACC model to show how the proposed MOO algorithm can be used to generate pareto-optimal deployments. A screen shot showing the integration of MOO algorithm into AutoFOCUS3 is shown in the figure \ref{pod}. As shown, there are 4 different optimization criteria that can be considered while generating the pareto-optimal deployments. The user can specify the values of variables $max\_nodes$, $max\_memory$ and $upper\_bound\_sil$ in the respective text boxes to form a constraint satisfaction problem. 

\begin{figure}[ht!]
	\centering
	\includegraphics[width=150mm]{figures/pod.png}
	\caption{Pareto-optimal deployment generation criteria \label{pod}}
\end{figure}

Given the logical architecture of ACC model (shown in figure \ref{la_acc}), wherein components \textit{SpeedPlausibilization}, \textit{DistancePlausibilization}, \textit{DistanceControl}, \textit{SpeedControl} and \textit{AccelerationControl} are marked as ASIL-C, ASIL-C, ASIL-B, ASIL-A and ASIL-A respectively, we generate a set of pareto-deployments assuming the values for variables \textit{max\_nodes}, \textit{max\_memory} and \textit{upper\_bound\_sil} as 4, 40 and 12 respectively. The set of all valid deployments generated for the ACC model are shown in the table \ref{table:valid_dep_acc}. \\

\begin{table}[h!]
	\centering
	\def\arraystretch{1}
	\begin{tabular}{ | l | l | l | l | l |}
		\hline
		Result & Nodes Used & Memory per node & E2E latency & Total SIL \\ \hline
		Deployment 1 & 2 &	30 & 46	& 4 \\ \hline  
		Deployment 2 & 2 &	40 & 46	& 4 \\ \hline  
		Deployment 3 & 3 &	20 & 51	& 7 \\ \hline
		Deployment 4 & 3 &	20 & 101 & 6 \\ \hline
		Deployment 5 & 3 &	20 & 60	& 7 \\ \hline
		Deployment 6 & 2 &	30 & 101 & 5 \\ \hline
		Deployment 7 & 2 &	30 & 70	& 5 \\ \hline
		Deployment 8 & 2 &	30 & 57	& 4 \\ \hline
		Deployment 9 & 4 &	20 & 60	& 9 \\ \hline
		Deployment 10 & 3 &	20 & 60	& 8 \\ \hline
		Deployment 11 & 3 &	20 & 60	& 6 \\ \hline
	\end{tabular}
	\caption{Valid deployments for ACC}
	\label{table:valid_dep_acc}
\end{table}

Once the set of valid deployments is generated, we filter out the deployments that do not get dominated by each other. The set of pareto-optimal deployments generated using the algorithm is shown in table \ref{table:pareto_dep_acc}. \\

\begin{table}[h!]
	\centering
	\def\arraystretch{1}
	\begin{tabular}{ | l | l | l | l | l |}
		\hline
		Result & Nodes Used & Memory per node & E2E latency & Total SIL \\ \hline
		Deployment 1 & 2 &	30 & 46	& 4 \\ \hline  
		Deployment 3 & 3 &	20 & 51	& 7 \\ \hline
		Deployment 11 & 3 &	20 & 60	& 6 \\ \hline
	\end{tabular}
	\caption{Pareto deployments for ACC}
	\label{table:pareto_dep_acc}
\end{table}

%
%\begin{figure}[ht!]
%	\centering
%	\includegraphics[width=150mm]{figures/pod_acc.png}
%	\caption{Pareto-optimal deployments for ACC system \label{pod_acc}}
%\end{figure} 
%

Once the pareto-optimal deployments are generated, the user can choose a deployment depending on which optimization criteria is prioritized by him. For e.g. if the user wants to prioritize the criteria \textit{total SIL} over other criteria, \textit{Deployment 1} looks the best option. These deployments can be saved and viewed in a \textit{scheduling view} of AutoFOCUS3. For e.g. the scheduling view of \textit{Deployment 1} is shown in the figure \ref{sched_view_acc}. \\

\begin{figure}[ht!]
	\centering
	\includegraphics[width=150mm]{figures/sched_view_acc.png}
	\caption{Scheduling view of Deployment 1 \label{sched_view_acc}}
\end{figure}

To conclude, we have introduced a simple MOO algorithm that optimizes 4 different criteria; number of nodes, memory per node, total SIL and e2e-latency. Using this algorithm, the user can generate a set of pareto-optimal deployments from the set of valid deployments, thereby, providing less, but better alternatives. 
    