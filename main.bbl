% $ biblatex auxiliary file $
% $ biblatex version 2.4 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\entry{maxsat}{article}{}
  \name{author}{2}{}{%
    {{}%
     {Argelich}{A.}%
     {Josep}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Manyà}{M.}%
     {Felip}{F.}%
     {}{}%
     {}{}}%
  }
  \list{language}{1}{%
    {English}%
  }
  \list{publisher}{1}{%
    {Kluwer Academic Publishers}%
  }
  \keyw{Soft constraints; Max-SAT; Solvers}
  \strng{namehash}{AJMF1}
  \strng{fullhash}{AJMF1}
  \field{labelalpha}{AM06}
  \field{sortinit}{A}
  \verb{doi}
  \verb 10.1007/s10732-006-7234-9
  \endverb
  \field{issn}{1381-1231}
  \field{number}{4-5}
  \field{pages}{375\bibrangedash 392}
  \field{title}{Exact Max-SAT solvers for over-constrained problems}
  \verb{url}
  \verb http://dx.doi.org/10.1007/s10732-006-7234-9
  \endverb
  \field{volume}{12}
  \field{journaltitle}{Journal of Heuristics}
  \field{year}{2006}
\endentry

\entry{SAFE_D333b}{report}{}
  \name{author}{8}{}{%
    {{}%
     {Adler}{A.}%
     {Nico}{N.}%
     {}{}%
     {}{}}%
    {{}%
     {Otten}{O.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Metzker}{M.}%
     {Eduard}{E.}%
     {}{}%
     {}{}}%
    {{}%
     {Peikenkamp}{P.}%
     {Thomas}{T.}%
     {}{}%
     {}{}}%
    {{}%
     {Oertel}{O.}%
     {Markus}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Baumgart}{B.}%
     {Andreas}{A.}%
     {}{}%
     {}{}}%
    {{}%
     {Voss}{V.}%
     {Sebastian}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Khalil}{K.}%
     {Maged}{M.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{ANOSME+1}
  \strng{fullhash}{ANOSMEPTOMBAVSKM1}
  \field{labelalpha}{AOM+13}
  \field{sortinit}{A}
  \field{note}{Available from \url{https://itea3.org/project/safe.html}}
  \field{title}{Deliverable D3.3.3b: Final specification for comparison of
  architecture}
  \list{institution}{1}{%
    {ITEA Consortium}%
  }
  \field{type}{Project Deliverable}
  \field{year}{2013}
\endentry

\entry{azevedo2014exploring}{incollection}{}
  \name{author}{6}{}{%
    {{}%
     {Azevedo}{A.}%
     {Lu{\'\i}s~Silva}{L.~S.}%
     {}{}%
     {}{}}%
    {{}%
     {Parker}{P.}%
     {David}{D.}%
     {}{}%
     {}{}}%
    {{}%
     {Papadopoulos}{P.}%
     {Yiannis}{Y.}%
     {}{}%
     {}{}}%
    {{}%
     {Walker}{W.}%
     {Martin}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Sorokos}{S.}%
     {Ioannis}{I.}%
     {}{}%
     {}{}}%
    {{}%
     {Ara{\'u}jo}{A.}%
     {Rui~Esteves}{R.~E.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Springer}%
  }
  \strng{namehash}{ALSPDPY+1}
  \strng{fullhash}{ALSPDPYWMSIARE1}
  \field{labelalpha}{APP+14}
  \field{sortinit}{A}
  \field{booktitle}{Model-Based Safety and Assessment}
  \field{pages}{70\bibrangedash 81}
  \field{title}{Exploring the Impact of Different Cost Heuristics in the
  Allocation of Safety Integrity Levels}
  \field{year}{2014}
\endentry

\entry{Broy:2001:SDI:374869}{book}{}
  \name{author}{2}{}{%
    {{}%
     {Broy}{B.}%
     {Manfred}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {St{\o}len}{S.}%
     {Ketil}{K.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Springer-Verlag New York, Inc.}%
  }
  \strng{namehash}{BMSK1}
  \strng{fullhash}{BMSK1}
  \field{labelalpha}{BS01}
  \field{sortinit}{B}
  \field{isbn}{0-387-95073-7}
  \field{title}{Specification and Development of Interactive Systems: Focus on
  Streams, Interfaces, and Refinement}
  \list{location}{1}{%
    {Secaucus, NJ, USA}%
  }
  \field{year}{2001}
\endentry

\entry{SAFE_D44b}{report}{}
  \name{author}{3}{}{%
    {{}%
     {Chaudhary}{C.}%
     {Mayank}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Khalil}{K.}%
     {Maged}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Zverlov}{Z.}%
     {Sergey}{S.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{CMKMZS1}
  \strng{fullhash}{CMKMZS1}
  \field{labelalpha}{CKZ14}
  \field{sortinit}{C}
  \field{note}{Available from \url{https://itea3.org/project/safe.html}}
  \field{title}{Deliverable D4.4b: Final version of plug-in for safety and
  multi criteria architecture modeling and benchmarking}
  \list{institution}{1}{%
    {ITEA Consortium}%
  }
  \field{type}{Project Deliverable}
  \field{year}{2014}
\endentry

\entry{Dittel:2010:SSC:1886301.1886311}{inproceedings}{}
  \name{author}{2}{}{%
    {{}%
     {Dittel}{D.}%
     {Torsten}{T.}%
     {}{}%
     {}{}}%
    {{}%
     {Aryus}{A.}%
     {Hans-J\"{o}rg}{H.-J.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Springer-Verlag}%
  }
  \keyw{ASIL, FMDEA, FTA, ISO 26262, PHA, lane assistance functions, safety
  case, safety concept, safety requirements, validation and verification}
  \strng{namehash}{DTAHJ1}
  \strng{fullhash}{DTAHJ1}
  \field{labelalpha}{DA10}
  \field{sortinit}{D}
  \field{booktitle}{Proceedings of the 29th International Conference on
  Computer Safety, Reliability, and Security}
  \field{isbn}{3-642-15650-9, 978-3-642-15650-2}
  \field{pages}{97\bibrangedash 111}
  \field{series}{SAFECOMP'10}
  \field{title}{How to "Survive" a Safety Case According to ISO 26262}
  \verb{url}
  \verb http://dl.acm.org/citation.cfm?id=1886301.1886311
  \endverb
  \list{location}{1}{%
    {Vienna, Austria}%
  }
  \field{year}{2010}
  \warn{\item Can't use 'location' + 'address'}
\endentry

\entry{deb2001multi}{book}{}
  \name{author}{1}{}{%
    {{}%
     {Deb}{D.}%
     {Kalyanmoy}{K.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {John Wiley \& Sons}%
  }
  \strng{namehash}{DK1}
  \strng{fullhash}{DK1}
  \field{labelalpha}{Deb01}
  \field{sortinit}{D}
  \field{title}{Multi-objective optimization using evolutionary algorithms}
  \field{volume}{16}
  \field{year}{2001}
\endentry

\entry{DeMoura:2008:ZES:1792734.1792766}{inproceedings}{}
  \name{author}{2}{}{%
    {{}%
     {De~Moura}{D.~M.}%
     {Leonardo}{L.}%
     {}{}%
     {}{}}%
    {{}%
     {Bj{\o}rner}{B.}%
     {Nikolaj}{N.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Springer-Verlag}%
  }
  \strng{namehash}{DMLBN1}
  \strng{fullhash}{DMLBN1}
  \field{labelalpha}{DMB08}
  \field{sortinit}{D}
  \field{booktitle}{Proceedings of the Theory and Practice of Software, 14th
  International Conference on Tools and Algorithms for the Construction and
  Analysis of Systems}
  \field{isbn}{3-540-78799-2, 978-3-540-78799-0}
  \field{pages}{337\bibrangedash 340}
  \field{series}{TACAS'08/ETAPS'08}
  \field{title}{Z3: An Efficient SMT Solver}
  \verb{url}
  \verb http://dl.acm.org/citation.cfm?id=1792734.1792766
  \endverb
  \list{location}{1}{%
    {Budapest, Hungary}%
  }
  \field{year}{2008}
  \warn{\item Can't use 'location' + 'address'}
\endentry

\entry{fu2006solving}{incollection}{}
  \name{author}{2}{}{%
    {{}%
     {Fu}{F.}%
     {Zhaohui}{Z.}%
     {}{}%
     {}{}}%
    {{}%
     {Malik}{M.}%
     {Sharad}{S.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Springer}%
  }
  \strng{namehash}{FZMS1}
  \strng{fullhash}{FZMS1}
  \field{labelalpha}{FM06}
  \field{sortinit}{F}
  \field{booktitle}{Theory and Applications of Satisfiability Testing-SAT 2006}
  \field{pages}{252\bibrangedash 265}
  \field{title}{On solving the partial MAX-SAT problem}
  \field{year}{2006}
\endentry

\entry{247892}{article}{}
  \name{author}{1}{}{%
    {{}%
     {Gu}{G.}%
     {Jun}{J.}%
     {}{}%
     {}{}}%
  }
  \keyw{computational complexity;formal logic;optimisation;search problems;VLSI
  engineering;average time complexity analysis;computing theory;conjunctive
  normal form formulas;constraint satisfaction;iterative
  optimization;mathematical logic;objective function;satisfiability
  problem;unconstrained optimization;Algorithm design and
  analysis;Connectors;Constraint theory;Helium;Iterative
  algorithms;Logic;Machine learning;NP-complete problem;Performance
  analysis;Very large scale integration}
  \strng{namehash}{GJ1}
  \strng{fullhash}{GJ1}
  \field{labelalpha}{Gu93}
  \field{sortinit}{G}
  \verb{doi}
  \verb 10.1109/21.247892
  \endverb
  \field{issn}{0018-9472}
  \field{number}{4}
  \field{pages}{1108\bibrangedash 1129}
  \field{title}{Local search for satisfiability (SAT) problem}
  \field{volume}{23}
  \field{journaltitle}{Systems, Man and Cybernetics, IEEE Transactions on}
  \field{year}{1993}
  \warn{\item Invalid format of field 'month'}
\endentry

\entry{Holzl:2007:AST:1927558.1927576}{inproceedings}{}
  \name{author}{2}{}{%
    {{}%
     {H\"{o}lzl}{H.}%
     {Florian}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Feilkas}{F.}%
     {Martin}{M.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Springer-Verlag}%
  }
  \strng{namehash}{HFFM1}
  \strng{fullhash}{HFFM1}
  \field{labelalpha}{HF10}
  \field{sortinit}{H}
  \field{booktitle}{Proceedings of the 2007 International Dagstuhl Conference
  on Model-based Engineering of Embedded Real-time Systems}
  \field{isbn}{3-642-16276-2, 978-3-642-16276-3}
  \field{pages}{317\bibrangedash 322}
  \field{series}{MBEERTS'07}
  \field{title}{AutoFocus 3: A Scientific Tool Prototype for Model-based
  Development of Component-based, Reactive, Distributed Systems}
  \verb{url}
  \verb http://dl.acm.org/citation.cfm?id=1927558.1927576
  \endverb
  \list{location}{1}{%
    {Dagstuhl Castle, Germany}%
  }
  \field{year}{2010}
  \warn{\item Can't use 'location' + 'address'}
\endentry

\entry{Hoelzl10autofocustool}{misc}{}
  \name{author}{3}{}{%
    {{}%
     {Hölzl}{H.}%
     {Florian}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Spichkova}{S.}%
     {Maria}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Trachtenherz}{T.}%
     {David}{D.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{HFSMTD1}
  \strng{fullhash}{HFSMTD1}
  \field{labelalpha}{HST10}
  \field{sortinit}{H}
  \field{title}{AutoFocus Tool Chain}
  \field{year}{2010}
\endentry

\entry{Kang:2010:AED:2023011.2023014}{inproceedings}{}
  \name{author}{3}{}{%
    {{}%
     {Kang}{K.}%
     {Eunsuk}{E.}%
     {}{}%
     {}{}}%
    {{}%
     {Jackson}{J.}%
     {Ethan}{E.}%
     {}{}%
     {}{}}%
    {{}%
     {Schulte}{S.}%
     {Wolfram}{W.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Springer-Verlag}%
  }
  \strng{namehash}{KEJESW1}
  \strng{fullhash}{KEJESW1}
  \field{labelalpha}{KJS11}
  \field{sortinit}{K}
  \field{booktitle}{Proceedings of the 16th Monterey Conference on Foundations
  of Computer Software: Modeling, Development, and Verification of Adaptive
  Systems}
  \field{isbn}{978-3-642-21291-8}
  \field{pages}{33\bibrangedash 54}
  \field{series}{FOCS'10}
  \field{title}{An Approach for Effective Design Space Exploration}
  \verb{url}
  \verb http://dl.acm.org/citation.cfm?id=2023011.2023014
  \endverb
  \list{location}{1}{%
    {Redmond, WA}%
  }
  \field{year}{2011}
  \warn{\item Can't use 'location' + 'address'}
\endentry

\entry{Knight:2002:SCS:581339.581406}{inproceedings}{}
  \name{author}{1}{}{%
    {{}%
     {Knight}{K.}%
     {John~C.}{J.~C.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {ACM}%
  }
  \strng{namehash}{KJC1}
  \strng{fullhash}{KJC1}
  \field{labelalpha}{Kni02}
  \field{sortinit}{K}
  \field{booktitle}{Proceedings of the 24th International Conference on
  Software Engineering}
  \verb{doi}
  \verb 10.1145/581339.581406
  \endverb
  \field{isbn}{1-58113-472-X}
  \field{pages}{547\bibrangedash 550}
  \field{series}{ICSE '02}
  \field{title}{Safety Critical Systems: Challenges and Directions}
  \verb{url}
  \verb http://doi.acm.org/10.1145/581339.581406
  \endverb
  \list{location}{1}{%
    {Orlando, Florida}%
  }
  \field{year}{2002}
  \warn{\item Can't use 'location' + 'address'}
\endentry

\entry{marques2007using}{article}{}
  \name{author}{2}{}{%
    {{}%
     {Marques-Silva}{M.-S.}%
     {Joao}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Planes}{P.}%
     {Jordi}{J.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{MSJPJ1}
  \strng{fullhash}{MSJPJ1}
  \field{labelalpha}{MSP07}
  \field{sortinit}{M}
  \field{title}{On using unsatisfiability for solving maximum satisfiability}
  \field{journaltitle}{arXiv preprint arXiv:0712.1097}
  \field{year}{2007}
\endentry

\entry{NeemaSztipanovitsKarsai03_ConstraintBasedDesignSpaceExplorationModelSynthesis}{inproceedings}{}
  \name{author}{3}{}{%
    {{}%
     {Neema}{N.}%
     {S.}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Sztipanovits}{S.}%
     {J.}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Karsai}{K.}%
     {G.}{G.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{NSSJKG1}
  \strng{fullhash}{NSSJKG1}
  \field{labelalpha}{NSK03}
  \field{sortinit}{N}
  \field{abstract}{%
  An important bottleneck in model-based design of embedded systems is the cost
  of constructing models. This cost can be significantly decreased by
  increasing the reuse of existing model components in the design process. This
  paper describes a tool suite, which has been developed for component-based
  model synthesis. The DESERT tool suite can be interfaced to existing modeling
  and analysis environments and can be inserted in various, domain specific
  design flows. The modeling component of DESERT supports the modeling of
  design spaces and the avoss2012schedulingutomated search for designs that
  meet structural requirements. DESERT has been introduced in automotive
  applications and proved to be useful in increasing design productivity.%
  }
  \field{booktitle}{Embedded Software Lecture Notes in Computer Science, Volume
  2855/2003}
  \field{pages}{290\bibrangedash 305}
  \field{title}{Constraint-Based Design-Space Exploration and Model Synthesis}
  \verb{url}
  \verb http://chess.eecs.berkeley.edu/pubs/743.html
  \endverb
  \field{year}{2003}
\endentry

\entry{part1}{standard}{}
  \field{labelalpha}{Par}
  \field{extraalpha}{1}
  \field{sortinit}{P}
  \field{title}{Road Vehicles - Functional Safety - Part 1: Vocabulary}
  \field{type}{ISO 26262-1}
\endentry

\entry{part3}{standard}{}
  \field{labelalpha}{Par}
  \field{extraalpha}{2}
  \field{sortinit}{P}
  \field{title}{Road Vehicles - Functional Safety - Part 3: Concept Phase}
  \field{type}{ISO 26262-3}
\endentry

\entry{part5}{standard}{}
  \field{labelalpha}{Par}
  \field{extraalpha}{3}
  \field{sortinit}{P}
  \field{title}{Road Vehicles - Functional Safety - Part 5: Product
  development: Hardware level}
  \field{type}{ISO 26262-5}
\endentry

\entry{part6}{standard}{}
  \field{labelalpha}{Par}
  \field{extraalpha}{4}
  \field{sortinit}{P}
  \field{title}{Road Vehicles - Functional Safety - Part 6: Product
  development: Software level}
  \field{type}{ISO 26262-6}
\endentry

\entry{5577992}{inproceedings}{}
  \name{author}{2}{}{%
    {{}%
     {Saxena}{S.}%
     {T.}{T.}%
     {}{}%
     {}{}}%
    {{}%
     {Karsai}{K.}%
     {G.}{G.}%
     {}{}%
     {}{}}%
  }
  \keyw{hardware-software codesign;systems analysis;DSE techniques;domain
  specific frameworks;generic design space exploration
  framework;hardware-software codesign;higher level approach;low-level
  constraint language;real-time software synthesis;software product
  lines;Computational modeling;Context;Encoding;Hardware;Mathematical
  model;Software;Space exploration;design space exploration}
  \strng{namehash}{STKG1}
  \strng{fullhash}{STKG1}
  \field{labelalpha}{SK10}
  \field{sortinit}{S}
  \field{booktitle}{Computer and Information Technology (CIT), 2010 IEEE 10th
  International Conference on}
  \verb{doi}
  \verb 10.1109/CIT.2010.330
  \endverb
  \field{pages}{1940\bibrangedash 1947}
  \field{title}{Towards a Generic Design Space Exploration Framework}
  \field{year}{2010}
  \warn{\item Invalid format of field 'month'}
\endentry

\entry{Schatz:2015:ADE:2744769.2747912}{inproceedings}{}
  \name{author}{3}{}{%
    {{}%
     {Sch\"{a}tz}{S.}%
     {Bernhard}{B.}%
     {}{}%
     {}{}}%
    {{}%
     {Voss}{V.}%
     {Sebastian}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Zverlov}{Z.}%
     {Sergey}{S.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {ACM}%
  }
  \strng{namehash}{SBVSZS1}
  \strng{fullhash}{SBVSZS1}
  \field{labelalpha}{SVZ15}
  \field{sortinit}{S}
  \field{booktitle}{Proceedings of the 52Nd Annual Design Automation
  Conference}
  \verb{doi}
  \verb 10.1145/2744769.2747912
  \endverb
  \field{isbn}{978-1-4503-3520-1}
  \field{pages}{99:1\bibrangedash 99:6}
  \field{series}{DAC '15}
  \field{title}{Automating Design-space Exploration: Optimal Deployment of
  Automotive SW-components in an ISO26262 Context}
  \verb{url}
  \verb http://doi.acm.org/10.1145/2744769.2747912
  \endverb
  \list{location}{1}{%
    {San Francisco, California}%
  }
  \field{year}{2015}
  \warn{\item Can't use 'location' + 'address'}
\endentry

\entry{fta_nasa}{misc}{}
  \name{author}{1}{}{%
    {{}%
     {Vesely}{V.}%
     {Bill}{B.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{VB1}
  \strng{fullhash}{VB1}
  \field{labelalpha}{Ves}
  \field{sortinit}{V}
  \field{note}{NASA HQ. Available from
  \url{http://www.hq.nasa.gov/office/codeq/risk/docs/ftacourse.pdf}}
  \field{title}{Fault tree analysis(FTA): Concepts and Applications}
\endentry

\entry{van2008handbook}{book}{}
  \name{author}{3}{}{%
    {{}%
     {Van~Harmelen}{V.~H.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Lifschitz}{L.}%
     {Vladimir}{V.}%
     {}{}%
     {}{}}%
    {{}%
     {Porter}{P.}%
     {Bruce}{B.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Elsevier}%
  }
  \strng{namehash}{VHFLVPB1}
  \strng{fullhash}{VHFLVPB1}
  \field{labelalpha}{VHLP08}
  \field{sortinit}{V}
  \field{title}{Handbook of knowledge representation}
  \field{volume}{1}
  \field{year}{2008}
\endentry

\entry{voss2012scheduling}{inproceedings}{}
  \name{author}{2}{}{%
    {{}%
     {Voss}{V.}%
     {Sebastian}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Sch{\"a}tz}{S.}%
     {Bernhard}{B.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{VSSB2}
  \strng{fullhash}{VSSB2}
  \field{labelalpha}{VS}
  \field{sortinit}{V}
  \field{title}{Scheduling shared memory multicore architectures in AUTOFOCUS 3
  using Satisfiability Modulo Theories}
\endentry

\entry{6601578}{inproceedings}{}
  \name{author}{2}{}{%
    {{}%
     {Voss}{V.}%
     {S.}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Schatz}{S.}%
     {B.}{B.}%
     {}{}%
     {}{}}%
  }
  \keyw{Boolean functions;computability;embedded systems;processor
  scheduling;programming language semantics;shared memory systems;Auto FOCUS3
  tool-chain;Boolean formulas;SMT;computation resources;deployment
  synthesis;discrete-time semantics;explicit data-flow semantics;global
  discrete time;linear arithmetic constraints;local memory constraints;message
  schedules;mixed-critical shared-memory applications;mixed-criticality
  multicore architectures;multicriticality embedded systems;satisfiability
  modulo theory solver;satisfiability problem;scheduling synthesis;system
  architecture;system architectures generation;task assignment
  optimization;task criticality constraints;task schedules;Computational
  modeling;Multicore processing;Processor scheduling;Resource
  management;Schedules;Scheduling;Deployment
  Synthesis;Mapping;SMT;Scheduling;Shared-Memory Applications}
  \strng{namehash}{VSSB1}
  \strng{fullhash}{VSSB1}
  \field{labelalpha}{VS13}
  \field{sortinit}{V}
  \field{booktitle}{Engineering of Computer Based Systems (ECBS), 2013 20th
  IEEE International Conference and Workshops on the}
  \verb{doi}
  \verb 10.1109/ECBS.2013.23
  \endverb
  \field{pages}{100\bibrangedash 109}
  \field{title}{Deployment and Scheduling Synthesis for Mixed-Critical
  Shared-Memory Applications}
  \field{year}{2013}
  \warn{\item Invalid format of field 'month'}
\endentry

\entry{6869555}{inproceedings}{}
  \name{author}{5}{}{%
    {{}%
     {Zulkifli}{Z.}%
     {S.A.}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Asirvadam}{A.}%
     {V.S.}{V.}%
     {}{}%
     {}{}}%
    {{}%
     {Saad}{S.}%
     {N.}{N.}%
     {}{}%
     {}{}}%
    {{}%
     {Aziz}{A.}%
     {A.R.A.}{A.}%
     {}{}%
     {}{}}%
    {{}%
     {Mohideen}{M.}%
     {A.A.M.}{A.}%
     {}{}%
     {}{}}%
  }
  \keyw{cables (mechanical);closed loop systems;electrical engineering
  computing;hybrid electric vehicles;internal combustion engines;position
  control;virtual instrumentation;ICE air intake manifold;National Instrument
  CompactRIO;air-fuel intake;built-in position sensor;closed-loop throttle
  position control;driver accelerator pedal;electromechanical device;electronic
  TBW;electronic throttle-by-wire;embedded CompactRIO controller;engine
  power;hybrid electric vehicle;internal combustion engine;mechanical
  cable;mechanical throttle valve body;power transistors;real-time
  LabVIEW;throttle body;throttle position control system;vehicle
  acceleration;vehicle propulsion system;Energy management;Engines;Hybrid
  electric vehicles;Nickel;Pulse width modulation;Tuning;CompactRIO;LabVIEW;PID
  control;electronic throttle;hybrid electric vehicle;throttle-by-wire}
  \strng{namehash}{ZSAVSN+1}
  \strng{fullhash}{ZSAVSNAAMA1}
  \field{labelalpha}{ZAS+14}
  \field{sortinit}{Z}
  \field{booktitle}{Intelligent and Advanced Systems (ICIAS), 2014 5th
  International Conference on}
  \verb{doi}
  \verb 10.1109/ICIAS.2014.6869555
  \endverb
  \field{pages}{1\bibrangedash 6}
  \field{title}{Implementation of electronic throttle-by-wire for a hybrid
  electric vehicle using National Instruments' CompactRIO and LabVIEW
  Real-Time}
  \field{year}{2014}
  \warn{\item Invalid format of field 'month'}
\endentry

\entry{6903157}{inproceedings}{}
  \name{author}{2}{}{%
    {{}%
     {Zverlov}{Z.}%
     {S.}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Voss}{V.}%
     {S.}{S.}%
     {}{}%
     {}{}}%
  }
  \keyw{Pareto optimisation;computer architecture;embedded systems;energy
  consumption;multiprocessing systems;tree searching;Auto FOCUS3;Pareto
  efficient technical architecture;application-specific homogeneous multicore
  architectures;branch & bound algorithm;component based structure
  view;computing power;embedded systems;energy consumption;logical
  architecture;model-based tooling framework;multicore systems;multiprocessor
  system;optimization criteria;single-core technology;task
  mapping;three-dimensional optimization problem;Energy
  consumption;Hardware;Multicore processing;Optimization;Schedules;Timing;Auto
  FOCUS;Branch &amp; Bound;Design Space Exploration;Multi-Core
  Architectures;Optimization;Pareto Efficiency;SMT}
  \strng{namehash}{ZSVS1}
  \strng{fullhash}{ZSVS1}
  \field{labelalpha}{ZV14}
  \field{sortinit}{Z}
  \field{booktitle}{Computer Software and Applications Conference Workshops
  (COMPSACW), 2014 IEEE 38th International}
  \verb{doi}
  \verb 10.1109/COMPSACW.2014.63
  \endverb
  \field{pages}{366\bibrangedash 371}
  \field{title}{Synthesis of Pareto Efficient Technical Architectures for
  Multi-core Systems}
  \field{year}{2014}
  \warn{\item Invalid format of field 'month'}
\endentry

\lossort
\endlossort

\endinput
