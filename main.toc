\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {american}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Abstract}{i}{chapter*.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Background}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Problem Statement}{2}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Related Work}{3}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Organization of thesis}{3}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Theoretical Background}{5}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}ISO 26262}{5}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Different parts of ISO 26262}{5}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Vocabulary}{8}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}Safety metrics in ISO 26262}{10}{subsection.2.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.4}Safety Analysis methods in ISO 26262}{11}{subsection.2.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.5}Evaluation of hardware architectural metrics}{12}{subsection.2.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.6}Evaluation of residual risk of safety goal violation}{16}{subsection.2.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}AutoFOCUS3}{18}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Semantics}{18}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Layers of abstraction}{19}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}Deployment and scheduling model of AutoFOCUS3}{20}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Multi-objective optimization}{21}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Pareto-optimality}{22}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}SMT Solver}{24}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Phase I: DSE Of Safety Metrics And Resource Constraints}{25}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Safety metrics and deployment constraints}{25}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}PMHF Node Constraint}{26}{subsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}PMHF Bus Constraint}{27}{subsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}Memory Constraint}{27}{subsection.3.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.4}Node usage Constraint}{28}{subsection.3.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.5}Cost Constraint}{28}{subsection.3.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.6}Energy Consumption Constraint}{29}{subsection.3.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Integration into AutoFOCUS3}{30}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Annotations}{31}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Aftermath of using new constraints on ACC model}{33}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Use of SMT solvers to generate deployments}{38}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}UnSAT Cores}{43}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Phase II: Pareto-Optimal Deployment Generation}{44}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}A new MOO Algorithms}{45}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Integration into AutoFOCUS3}{49}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Case Study And Results}{52}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Phase I Results}{53}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Cost Constraint}{54}{subsection.5.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Node usage constraint}{57}{subsection.5.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}End-2-end latency vs cost}{58}{subsection.5.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.4}Bus load vs Cost}{59}{subsection.5.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Phase II Results}{59}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Conclusion}{66}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Assumptions}{67}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Future Work}{69}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acronyms}{70}{section*.43}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline List of Figures}{72}{appendix*.45}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline List of Tables}{73}{appendix*.46}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Bibliography}{74}{appendix*.47}
